import Publisher from '../IndexPublisher';


describe('Index Publisher', () => {
  var publisher;

  beforeEach(() => {
    publisher = new Publisher("blabla");

    spyOn(publisher, 'publish').and.callFake((cmd, body) => Promise.resolve({body}));
  });

  it('search', () => {
    publisher.search('foo', {query:  {"match_all" : {}}});

    expect(publisher.publish).toHaveBeenCalledWith('index.search', {
      docTypes: ['foo'],
      source: {query:  {"match_all" : {}}},
      offset: 0,
      per_page: 20
    });
  });

  it('search with pagination', () => {
    publisher.search('foo', {query:  {"match_all" : {}}}, 0, 10);

    expect(publisher.publish).toHaveBeenCalledWith('index.search', {
      docTypes: ['foo'],
      source: {query:  {"match_all" : {}}},
      offset: 0,
      per_page: 10
    });
  });

  it('percolates', () => {
    publisher.percolate('foo', '#34:45', '#34:4555');

    expect(publisher.publish).toHaveBeenCalledWith('index.percolate', {
      parentId: '#34:4555',
      docType: 'foo',
      docId: '#34:45'
    });
  });
});




