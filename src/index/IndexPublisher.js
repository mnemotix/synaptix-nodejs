/**
 * This file is part of the synaptix-nodejs package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 28/04/2016
 */


/**
 * Class used to publish commands to the Index module on Synaptix.
 *
 *  @extends AbstractPublisher
 */
import AbstractPublisher from '../AbstractPublisher';

import request from 'request-promise-native';
import winston from 'winston-color';
//request.debug = true;

export default class IndexPublisher extends AbstractPublisher{
  getName(){
    return 'index';
  }

  /**
   * Search for nodes
   *
   * @param {string|string[]} docTypes Document index types to match.
   * @param {object} body ES query
   * @param {int} from
   * @param {int} size
   * @param fullResponse
   *
   * @param sourcePropName
   * @deprecated Use this::query instead
   *
   * @returns {Promise.<Node[],Error>}
   */
  search(docTypes, body, from = 0, size = 20, fullResponse = false, sourcePropName = "source"){
    return this.query({
      docTypes,
      query: body,
      from,
      size,
      fullResponse,
      sourcePropName
    })
  }

  /**
   * Ask index for something
   *
   * @param args
   */
  query(args){
    let {docTypes, query, from, size, fullResponse, sourcePropName} = args;

    if (typeof docTypes === 'string') {
      docTypes = [docTypes];
    }

    docTypes = docTypes.map(docType => docType.toLowerCase());

    let extra = {};

    if (query.sort) {
      for (let fieldName in query.sort) {
        extra.sortCriteria = {
          fieldName,
          desc: query.sort[fieldName].order === 'desc'
        }
      }
    }

    // let date = (new Date()).getMilliseconds();
    // winston.profile(date);
    if (process.env.ES_URL && process.env.ES_INDEX_NAME && query) {
      // winston.debug(`Direct ES requesting`, `${process.env.ES_URL}/${process.env.ES_INDEX_NAME}/${docTypes.join(',')}/_search`, JSON.stringify(query, null, ' '));

      return request({
        uri: `${process.env.ES_URL}/${process.env.ES_INDEX_NAME}/${docTypes.join(',')}/_search`,
        method: 'POST',
        json: true,
        body: query,
        headers: {
          'content-type': 'application/json'
        }
      }).then((result) => {
        return fullResponse ? result : result.hits
      }).catch(error => {
        winston.error(`Resp error to ES requesting ${process.env.ES_URL}/${process.env.ES_INDEX_NAME}/${docTypes.join(',')}/_search`, JSON.stringify(query, null, ' '));
        winston.error(JSON.stringify(error, null, ' '));
      });
    } else {
      return this.publish('index.search', {
        docTypes,
        [sourcePropName || 'source']: query,
        per_page: size,
        offset: from,
        ...extra
      })
        .then(result => {
          return fullResponse ? result : result.hits
        });
    }
  }

  /**
   * Percolate against document
   *
   * @param {string} docType Document index type to percolate.
   * @param {string} docId Document id against to percolate
   * @param {string} parentId Parent document if the relation is parent/child
   *
   * @returns {Promise.<Node[],Error>}
   */
  percolate(docType, docId, parentId){
    return this.publish('index.percolate', {
      docType: docType.toLowerCase(),
      docId,
      parentId
    });
  }
}