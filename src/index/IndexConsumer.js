/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 13/04/2016
 */

import AbstractConsumer from '../AbstractConsumer';

/**
 *  Class used to synchronize data throw AMQP bus.
 *
 *  @extends AbstractConsumer
 */

export default class IndexConsumer extends AbstractConsumer{

  getName(){
    return 'index';
  }

  getRoutingKeys(){
    return ['sync.index.node.*'];
  }

  onMessage(msg) {
    msg = JSON.parse(msg.content.toString());

    /* Example of message. Only document id is returned.
    {
      command: 'sync.index.node.created',
      sender: 'a037c5df-e8a9-4eb0-8911-22efd1749f2d',
      date: 1519033834722,
      body: {
        _id: 'Person:1691626112:17920',
        nodeType: 'Person',
        uri: 'mm:person:1516614519883',
        _origin: 'mnx:app:mm-carto-net',
        _source: {
           firstName: 'jimmy',
           lastName: 'thebot',
           lastUpdate: 1534854422526,
           fullName: 'jimmy thebot',
           creationDate: 1516614519952,
           _enabled: true
        },
        _meta: null
      }
    }
    */

    let nodeId = msg.body._id || msg.body;
    let nodeType = msg.body.nodeType || nodeId.slice(0, nodeId.indexOf(':'));

    switch (msg.command) {
      case "sync.index.node.created":
      case "sync.index.node.updated":
      case "sync.index.node.deleted":
        this.dispatchAfterDelay(3000, msg.command, nodeType, nodeId);
        break;
    }
  }

  bindOnCreate(callback) {
    this.bind("sync.index.node.created", callback);
  }

  bindOnUpdate(callback) {
    this.bind("sync.index.node.updated", callback);
  }

  bindOnDelete(callback) {
    this.bind("sync.index.node.deleted", callback);
  }
}