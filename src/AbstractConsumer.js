/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 13/04/2016
 */

import amqp from 'amqplib/callback_api';
import winston from 'winston-color';
import uuid from 'uuid/v4';

export default class {

  amqpURI = `amqp://${process.env.RABBITMQ_LOGIN}:${process.env.RABBITMQ_PASSWORD}@${process.env.RABBITMQ_HOST}`;
  exchangeName = process.env.RABBITMQ_EXCHANGE_NAME || 'synaptix-local';
  routingKeys = [];
  callbacks = [];

  getName(){
    return uuid();
  }

  constructor() {
    this.connect();
  }

  connect() {
    amqp.connect(this.amqpURI, (err, conn) => {

      if(!err) {
        conn.createChannel((err, ch) => {
          ch.assertExchange(this.exchangeName, 'topic', {durable: false});

          const resultsQueueName = `${process.env.UUID || uuid()}_pid:${process.pid}_consumer:${this.getName()}_queue`;

          ch.assertQueue(resultsQueueName, {exclusive: true}, (err, q) => {
            winston.info(`AMQP connection OK. Waiting AMQP messages on exchange : amqp://${this.exchangeName}/${q.queue}.`);

            this.getRoutingKeys()
              .map((routingKey) => ch.bindQueue(q.queue, this.exchangeName, routingKey));

            ch.consume(q.queue, this.onMessage.bind(this), {noAck: true});
          });
        });

        conn.on("error", (err) => this.onError(err, true));

      } else {
        this.onError(err, true);
      }

    });
  }

  onError (err, displayWarning = false) {
    if (displayWarning) {
      winston.warn(`AMQP connection lost ! Attempt reconnection every 5s...`, err);
    }

    setTimeout(this.connect.bind(this), 5000);
  }

  onMessage(msg){
    throw "You must override this method. This is the callback after having received a message.";
  }

  getRoutingKeys(){
    throw "You must override this method. This provides routing keys to select topics to listen to.";
  }

  dispatch(command, ...params){
    if(this.callbacks[command]) {
      this.callbacks[command].map(callback => callback(...params));
    }
  }

  dispatchAfterDelay(delay, command, ...params){
    if(this.callbacks[command]) {
      this.callbacks[command].map(callback => {
        setTimeout(() => {
          callback(...params)
        }, delay);
      });
    }
  }

  bind(command, callback) {
    if(!this.callbacks[command]) {
      this.callbacks[command] = []
    }

    this.callbacks[command].push(callback);
  }
}