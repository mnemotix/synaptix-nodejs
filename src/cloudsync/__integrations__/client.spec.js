import Client from '../CloudsyncClient';

process.env.CLOUD_BASE_URL = "http://127.0.0.1:8082";
process.env.TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiJjZWIwOThiNi0xMTE2LTRhMzAtYWYwMS0yMzBkZTI3NDU3ZTIiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNDY2NjY3MTc4LCJpc3MiOiJodHRwOi8vMTkyLjE2OC45OS4xMDA6ODA4MC9hdXRoL3JlYWxtcy9yZXNvdXJjZSIsImF1ZCI6Im93bmNsb3VkIiwic3ViIjoiMzA3ZGQ0YjUtMzAxZi00OTQ4LWIxMDUtYTBhMjBiMmRkMDBhIiwidHlwIjoiT2ZmbGluZSIsImF6cCI6Im93bmNsb3VkIiwic2Vzc2lvbl9zdGF0ZSI6IjE1OGNjZTk3LWQ1Y2UtNGEwYy1hMDM4LWJiNWM4N2NhNGRiOCIsImNsaWVudF9zZXNzaW9uIjoiMmYxOTIwNTUtM2U3NS00OWEyLWI0YzAtZDQwNjNiNmNjODg2IiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsInZpZXctcHJvZmlsZSJdfX19.B8XEUmmnuglanPnrw87qhzdT1anjthEPExEpCR12SYqVUFHuAd-1gbJoEVySPNSZmRGx2Pq-lPXvTKfYedXPCVcr7WHgo4OxM_OREMi3iM425zXOM0JnqGgBh__Mv0BfPdryla_l5oCL7s2VsA488Huf1RmJRc2dr3j5HRRuHBLAq2NcwnHQcFi1GGjTe1qOpsxfCgcpU-1GmQTdH5sGHar92d2miwvZDieXDvNEAgNlrYtXE9QsAvdLS7BU0GBfcEBPAZsh3joElof2GV1FfTw7MxlRww-rwbvxyrg2R7wdtVKlWcaKAgHBtokxwTJkQqGlnM37GfXvgGCCUd9bEw";

var client = new Client();

fdescribe("CloudsyncClient integration tests", () => {

  beforeEach(() => {
    client.refreshToken(process.env.TOKEN );
  });

  it('Put a file', (done) => {
    client.putFile("/test.txt", "Lorem ipsum test ++.")
      .then((rsp) => {
        console.log(rsp);
        done();
      })
      .catch((err) => {
        fail(err);
        done();
      });
  });

  it('Get a file', (done) => {
    client.getFile("/test.txt")
      .then((rsp) => {
        expect(rsp).toEqual("Lorem ipsum test ++.");
        done();
      })
      .catch((err) => {
        fail(err);
        done();
      });
  });

  it('Create a folder', (done) => {
    client.createFolder("/test_arbo")
      .then((rsp) => {
        done();
      })
      .catch((err) => {
        fail(err);
        done();
      });
  });


  it('Create a deep folder', (done) => {
    client.createFolder("/a/b/c/d")
      .then((rsp) => {
        done();
      })
      .catch((err) => {
        fail(err);
        done();
      });
  });

  fit('Put a file in arbo', (done) => {
    client.putFile("/b/c/d/e/test.txt", "Lorem ipsum test ++.")
      .then((rsp) => {
        done();
      })
      .catch((err) => {
        fail(err);
        done();
      });
  });

  it('Share a folder as public', (done) => {
    client.sharePublicResource("/test_arbo")
      .then((rsp) => {
        expect(typeof rsp).toEqual('string');
        done();
      })
      .catch((err) => {
        fail(err);
        done();
      });
  });

  it('Share a file as public', (done) => {
    client.sharePublicResource("/test_arbo/test.txt")
      .then((rsp) => {
        expect(typeof rsp).toEqual('string');
        done();
      })
      .catch((err) => {
        fail(err);
        done();
      });
  });

  xit('Delete a folder', (done) => {
    client.removeFolder("/test_arbo")
      .then((rsp) => {
        done();
      })
      .catch((err) => {
        fail(err);
        done();
      });
  });
});