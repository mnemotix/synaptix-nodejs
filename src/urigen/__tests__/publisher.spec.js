import Publisher from '../UrigenPublisher';

describe('UriGetPublisher', () => {
  var publisher;

  beforeEach(() => {
    publisher = new Publisher("blabla");

    spyOn(publisher, 'publish').and.callFake((cmd, message) => ({cmd, message}));
  });


  it('get an uri', () => {
    publisher.getUri('foo', 'bar');

    expect(publisher.publish).toHaveBeenCalledWith('uri.get', 'foo:bar:uri');
  });

  it('create an uri', () => {
    publisher.createUri('foo');

    expect(publisher.publish).toHaveBeenCalledWith('uri.create', { objectType: 'foo' });
  });

  it('delete an uri', () => {
    publisher.deleteUri('foo', 'bar');

    expect(publisher.publish).toHaveBeenCalledWith('uri.delete', 'foo:bar:*');
  });
});




