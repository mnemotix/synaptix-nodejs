/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 13/04/2016
 */

import publisher from './amqp/publisher';
import uuid from 'uuid/v4';
import winston from 'winston-color';

export default class {
  constructor(senderId) {
    this.senderId = senderId;
  }

  getName(){
    return uuid();
  }

  /**
   * Publish an RPC AMQP message.
   *
   * @param {string} command RPC command
   * @param {*} body RPC body
   * @param {object} context extra AMQP headers
   * @param options
   * @returns {Promise.<T>}
   */
  async publish(command, body, context = {}, options = {}) {
    if (this.initing) {
      return new Promise((resolve, reject) => {
        setTimeout(() => resolve(this.publish(command, body, context, options)), 200);
      });
    }

    if (!this.amqpPublisher){
      let host = process.env.RABBITMQ_HOST || 'localhost';
      let port = process.env.RABBITMQ_PORT || 5672;
      let login = process.env.RABBITMQ_LOGIN || 'guest';
      let password = process.env.RABBITMQ_PASSWORD || 'guest';
      let exchangeName = process.env.RABBITMQ_EXCHANGE_NAME || 'synaptix-local';

      this.initing = true;
      this.amqpPublisher = await publisher({
        publisherName: this.getName() + '-' + uuid(),
        host,
        port,
        login,
        password,
        exchangeName,
        ttl: 30e3,
        ...options
      });

      winston.info(`Synaptix module [${this.getName()}] is connecting to broker with uri [rabbitmq://${login}:${password}@${host}:${port}/${exchangeName}] [OK]`);

      this.initing = false;
    }

    return new Promise((resolve, reject) => {
      let payload = {
        command,
        sender: this.senderId || process.env.UUID || 'mnx:app:nodejs',
        date: Date.now(),
        context,
        body
      };

      this.amqpPublisher.call(command, payload, this.onCallResponse.bind(this, resolve, reject, payload));
    })
  }

  onCallResponse(resolve, reject, payload, rsp) {
    //winston.debug('[cmd]', payload);

    if (rsp && rsp.data) {
      let data = JSON.parse(rsp.data);

      //winston.debug('[resp ok]', data);

      if(data && typeof data === "object") {
        // Is the response successful ?
        if (['ok', 'empty'].indexOf(data.status.toLowerCase()) !== -1) {
          resolve(data.body, rsp);
        } else {
          reject(JSON.stringify({command: data.command, requestPayload: payload.body, status : data.status, error : data.body}, null, " "));
        }
      }
    }
      //winston.debug('[resp err]', rsp);

      reject(JSON.stringify({command: payload.command, status: 'no_response', requestPayload: payload, error: {message : `Something is wrong with the RPC response :
${rsp}`}}, null, " "));
  }
}