/**
 * This file is part of the synaptix-nodejs package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 15/04/2016
 */

export default class {
  /** @type {string} id */
  id;
  /** @type {list} node label */
  nodeLabels;
  /** @type {boolean} Is IN direction ? OUT otherwise. */
  inDirection = true;
  /** @type {list} edge labels to filter on */
  edgeLabels;
  /** @type {list} node attributes to filter on */
  nodeAttributes;
  /** @type {number} pagination size */
  size;
  /** @type {number} pagination offset */
  from;

  /**
   * @constructor
   *
   * @param id
   * @param nodeLabels
   * @param inDirection
   * @param edgeLabels
   * @param nodeAttributes
   * @param size
   * @param from
   */
  constructor(id, nodeLabels, inDirection, edgeLabels, nodeAttributes = [], size = null, from = 0){
    this.id = id;
    this.nodeLabels = nodeLabels;
    this.inDirection = inDirection;
    this.edgeLabels = edgeLabels;
    this.nodeAttributes = nodeAttributes;
    this.size = size;
    this.from = from;
  }


  serialize(){
    var pagination = this.size && this.from ? {
      size: this.size,
      from: this.from
    } : {};

    return {
      id: this.id,
      nodeLabels: this.nodeLabels || [],
      inDirection: this.inDirection,
      edgeLabels: this.edgeLabels || [],
      nodeAttributes: this.nodeAttributes || [],
      ...pagination
    };
  }
}