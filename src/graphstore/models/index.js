/**
 * This file is part of the synaptix-nodejs package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 15/04/2016
 */

import Node from './Node';
import Edge from './Edge';
import Filter from './Filter';


function createEdge(subj, pred, obj, properties) {
  return new Edge(subj, pred, obj, properties);
}

function createNode(nodeType, id, properties, meta, uri){
  return new Node(nodeType, id, properties, meta, uri);
}

export {
  Node,
  Edge,
  Filter,
  createEdge,
  createNode
}