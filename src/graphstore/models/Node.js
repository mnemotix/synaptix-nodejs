/**
 * This file is part of the synaptix-nodejs package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 15/04/2016
 */

import Abstract from './Abstract';

export default class Node extends Abstract {
  /** @type {string} node type */
  nodeType;
  /** @type {string} uri */
  uri;

  /**
   * @constructor
   *
   * @param nodeType
   * @param id
   * @param properties
   * @param meta
   * @param uri
   */
  constructor(nodeType, id, properties, meta, uri) {
    super(id, properties, meta);

    this.nodeType = nodeType;
    this.uri = uri;
  }

  getNodeType() {
    return this.nodeType;
  }

  /**
   * Get URI
   */
  getUri() {
    return this.uri;
  }

  /**
   * Serialize Node into a JSON compatible format.
   *
   * @returns {{nodeType: string, id: string, properties: list}}
   */
  serialize(){
    return {
      nodeType: this.nodeType,
      _id: this.id,
      _origin: Abstract.getOrigin(),
      _source: {
        ...this.properties
      },
      uri: this.uri
    };
  }

  /**
   * Factory to create a node
   *
   * @param nodeType
   * @param id
   * @param properties
   * @param meta
   * @param uri
   * @returns {Node}
   */
  static createNode(nodeType, id, properties, meta, uri){
    return new Node(nodeType, id, properties, meta, uri);
  }


  /**
   * Factory to create a partial node serialization without id
   *
   * @param {string} nodeType Node type
   * @param {string} uri Node URI
   * @param {object} properties Additionals node properties to store.
   * @param {object} meta Additionals meta to consolidate
   */
  static serializePartial(nodeType, uri, properties = {}, meta = null){
    if (typeof properties._enabled === 'undefined') {
      properties._enabled = true;
    }

    let _meta = meta ? { _meta : meta } : {};

    return {
      nodeType: Node.normalizedNodeType(nodeType),
      uri,
      _origin: Abstract.getOrigin(),
      _source: properties,
      ..._meta
    };
  }

  /**
   * Factory to create a node serialization
   *
   * @param {string} nodeType Node type
   * @param {string} id Node id
   * @param {string} uri Node URI
   * @param {object} properties Additionals node properties to store.
   * @param {object} meta Additionals meta to consolidate
   */
  static serializeNode(nodeType, id, uri, properties = {}, meta = null){
    let _meta = meta ? { _meta : meta } : {};

    return {
      nodeType: Node.normalizedNodeType(nodeType),
      _id: id,
      _origin: Abstract.getOrigin(),
      _source: properties,
      uri,
      ..._meta
    };
  }

  static normalizedNodeType(nodeType) {
    return nodeType.charAt(0).toUpperCase() + nodeType.slice(1);
  }
}