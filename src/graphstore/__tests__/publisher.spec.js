import Publisher from '../GraphstorePublisher';

import {createEdge, createNode, Node, Edge} from '../models';

describe('GraphStore Publisher DSE', () => {
  var publisher;

  beforeEach(() => {
    publisher = new Publisher("blabla");

    spyOn(publisher, 'publish').and.callFake((cmd, body) => Promise.resolve({body}));
  });

  it('get a node by id', () => {
    publisher.getNode('foo');

    expect(publisher.publish).toHaveBeenCalledWith('nodes.select', { query: "g.V('foo').has('_enabled', true) " }, undefined);
  });

  it('get a node by URI', () => {
    publisher.getNodeByURI('http://blabla.com/#foo');

    expect(publisher.publish).toHaveBeenCalledWith('nodes.select', { query: "g.V().has('uri','http://blabla.com/#foo').has('_enabled', true)" }, undefined);
  });


  it('create a node', () => {
    publisher.createNode('concept', 'http://blabla.com/#foo', { foo: 'bar'});

    expect(publisher.publish).toHaveBeenCalledWith('node.create', {
      nodeType: 'Concept',
      uri: 'http://blabla.com/#foo',
      _origin: 'mnx:app:nodejs',
      _source : {
        foo: 'bar',
        _enabled: true
      }
    }, undefined);
  });

  it('create a node with metas', () => {
    publisher.createNode('concept', 'http://blabla.com/#foo', { foo: 'bar'}, { bar: 'faz'});

    expect(publisher.publish).toHaveBeenCalledWith('node.create', {
      nodeType: 'Concept',
      uri: 'http://blabla.com/#foo',
      _origin: 'mnx:app:nodejs',
      _source : {
        foo: 'bar',
        _enabled: true
      },
      _meta: {
        bar: 'faz'
      }
    }, undefined);
  });

  it('update a node', () => {
    publisher.updateNode('concept', '#1:2', {foo: 'baz'});

    expect(publisher.publish).toHaveBeenCalledWith('node.update', {
      _id: '#1:2',
      nodeType: 'Concept',
      _origin: 'mnx:app:nodejs',
      _source : {
        foo: 'baz'
      }
    }, undefined);
  });

  it('delete a node', () => {
    publisher.deleteNode('#1:2');

    expect(publisher.publish).toHaveBeenCalledWith('node.delete', '#1:2', undefined);
  });

  it('create a edge', () => {
    publisher.createEdge('http://blabla.com/#foo', 'foo:bar', 'http://blabla.com/#bar');

    expect(publisher.publish).toHaveBeenCalledWith('edge.create', {
      subj: 'http://blabla.com/#foo',
      pred: 'FOO:BAR',
      obj: 'http://blabla.com/#bar',
      _origin: 'mnx:app:nodejs',
      _source: {
        _enabled: true
      }
    }, undefined);
  });

  it('delete a edge', () => {
    publisher.deleteEdge('#2:3');

    expect(publisher.publish).toHaveBeenCalledWith('edge.delete', '#2:3', undefined);
  });

  it('delete edges', () => {
    publisher.deleteEdges('#2:3', 'foo:bar', '#2:4');

    expect(publisher.publish).toHaveBeenCalledWith('edges.delete', {
      subj: '#2:3',
      pred: 'FOO:BAR',
      obj: '#2:4',
      _origin: 'mnx:app:nodejs',
      _source: {_enabled: true}
    }, undefined);
  });

  it('create a graph', () => {
    publisher.createGraph([
      Node.serializePartial('concept', 'http://ns.mnx/#1'),
      Node.serializePartial('concept', 'http://ns.mnx/#2'),
      Node.serializePartial('concept', 'http://ns.mnx/#3')
    ], [
      Edge.serializePartial('_:0', 'Narrower', '_:1'),
      Edge.serializePartial('_:1', 'Narrower', '_:2')
    ]);

    expect(publisher.publish).toHaveBeenCalledWith('graph.create', {
      nodes: [
        {
          nodeType: 'Concept',
          uri: 'http://ns.mnx/#1',
          _origin: 'mnx:app:nodejs',
          _source : {
            _enabled: true,
          }
        },
        {
          nodeType: 'Concept',
          uri: 'http://ns.mnx/#2',
          _origin: 'mnx:app:nodejs',
          _source : {
            _enabled: true,
          }
        },
        {
          nodeType: 'Concept',
          uri: 'http://ns.mnx/#3',
          _origin: 'mnx:app:nodejs',
          _source : {
            _enabled: true,
          }
        }
      ],
      edges: [
        {
          subj: '_:0',
          pred: 'NARROWER',
          obj: '_:1',
          _origin: 'mnx:app:nodejs',
          _source: {
            _enabled: true,
          }
        },
        {
          subj: '_:1',
          pred: 'NARROWER',
          obj: '_:2',
          _origin: 'mnx:app:nodejs',
          _source: {
            _enabled: true,
          }
        }
      ]
    }, undefined);
  });
});




