 /**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 13/04/2016
 */

import AbstractConsumer from '../AbstractConsumer';

import {createEdge, createNode, Node, Edge, Filter} from './models';

/**
 *  Class used to synchronize data throw AMQP bus.
 *
 *  @extends AbstractConsumer
 */

export default class GraphstoreConsumer extends AbstractConsumer{

  getName(){
    return 'graphstore';
  }

  getRoutingKeys(){
    return ['sync.node.*'];
  }

  onMessage(msg) {
    msg = JSON.parse(msg.content.toString());

    let data = msg.body;

    switch (msg.command) {
      case "sync.node.created":
      case "sync.node.updated":
        this.dispatch(msg.command, createNode(data.nodeType, data._id, data._source, data._meta, data.uri));
        break;
      case "sync.node.deleted":
        this.dispatch(msg.command, createNode(data.nodeType, data._id, data._source, data._meta, data.uri));
        break;
    }
  }

  bindOnCreate(callback) {
    this.bind("sync.node.created", callback);
  }

  bindOnUpdate(callback) {
    this.bind("sync.node.updated", callback);
  }

  bindOnDelete(callback) {
    this.bind("sync.node.deleted", callback);
  }
}