import UriGenPublisher from './urigen/UrigenPublisher';
import GraphStoreDsePublisher from './graphstore/GraphstorePublisher';
import GraphStoreDseConsumer from './graphstore/GraphstoreConsumer';
import IndexPublisher from './index/IndexPublisher';
import IndexConsumer from './index/IndexConsumer';
import KeycloakClient from "./keycloak/KeycloakClient";
import CloudsyncClient from "./cloudsync/CloudsyncClient";

import {createEdge as createDseEdge, createNode as createDseNode, Edge as DseEdge, Node as DseNode} from "./graphstore/models";

const
  uriGenPublisher     = new UriGenPublisher(process.env.UUID),
  graphStoreDsePublisher = new GraphStoreDsePublisher(process.env.UUID),
  indexPublisher      = new IndexPublisher(process.env.UUID),
  keycloakClient      = new KeycloakClient(process.env.OAUTH_REALM, process.env.UUID),
  cloudsyncClient     = new CloudsyncClient(process.env.UUID);

export {
  UriGenPublisher,
  GraphStoreDsePublisher,
  GraphStoreDseConsumer,
  IndexPublisher,
  IndexConsumer,
  KeycloakClient,
  CloudsyncClient,
  uriGenPublisher,
  graphStoreDsePublisher,
  indexPublisher,
  keycloakClient,
  cloudsyncClient,

  createDseEdge, createDseNode, DseEdge, DseNode
}