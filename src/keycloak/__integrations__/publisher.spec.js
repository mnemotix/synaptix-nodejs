import Client from '../KeycloakClient';

process.env.OAUTH_BASE_URL = "http://192.168.99.100:8080";

const REALMS  = 'integration_tests';
const CLIENT =  'test';

var client = new Client(REALMS, CLIENT);

describe("Keycloak client integration tests", () => {
  beforeEach((done) => {
    client.requestToken('admin', 'admin')
     .then(data => {
       client.refreshToken(data.access_token);
       done();
     })
      .catch(err => {
        console.log(err);
        done();
      });
  });


  it('gets clients', (done) => {
    client.getClients()
      .then(clients => {
        expect(clients.length).toEqual(6);
        done();
      });
  });

  var availableUsers;

  it('gets users', (done) => {
    client.getUsers()
      .then(users => {
        availableUsers = users;

        expect(users.length).toEqual(2);
        done();
      });
  });

  it('gets a user', (done) => {
    client.getUser(availableUsers[0].id)
      .then(user => {
        expect(user.id).toEqual(availableUsers[0].id);
        done();
      });
  });

  it('tests that is user admin', (done) => {
    client.isUserAdmin(availableUsers[0].id)
      .then(isAdmin => {
        expect(isAdmin).toEqual(true);
        done();
      });
  });

  it('tests that user not admin', (done) => {
    client.isUserAdmin(availableUsers[1].id)
      .then(isAdmin => {
        expect(isAdmin).toEqual(false);
        done();
      });
  });

  it('get avalailable roles for client', (done) => {
    client.getAvailableRolesForClient(availableUsers[0].id, CLIENT)
      .then(roles => {
        expect(roles.length).toEqual(1);
        done();
      });
  });

  it('gets available roles for client', (done) => {
    client.getAvailableRolesForClient(availableUsers[0].id, CLIENT)
      .then(roles => {
        expect(roles.length).toEqual(1);
        done();
      });
  });

  it('gets available roles for realm for admin user', (done) => {
    client.getAvailableRolesForRealm(availableUsers[0].id)
      .then(roles => {
        expect(roles.length).toEqual(2);
        done();
      });
  });

  it('gets available roles for realm for guest user', (done) => {
    client.getAvailableRolesForRealm(availableUsers[1].id)
      .then(roles => {
        expect(roles.length).toEqual(1);
        done();
      });
  });

  it('sets a role to user', (done) => {
    client.setRealmRoleToUser(availableUsers[0].id, 'guest')
      .then(() => {
        client.getAvailableRolesForRealm(availableUsers[0].id)
          .then(roles => {
            expect(roles.length).toEqual(1);
            done();
          });
      });
  });

  it('try to set twice a role to user', (done) => {
    client.setRealmRoleToUser(availableUsers[0].id, 'guest')
      .then(() => {
        fail("Should not pas here");
      })
      .catch(done);
  });

  it('remove a role to user', (done) => {
    client.removeRealmRoleForUser(availableUsers[0].id, 'guest')
      .then(() => {
        client.getAvailableRolesForRealm(availableUsers[0].id)
          .then(roles => {
            expect(roles.length).toEqual(2);
            done();
          });
      });
  });

  it('sets a user as admin', (done) => {
    client.setAdminRoleToUser(availableUsers[1].id)
      .then(() => {
        client.isUserAdmin(availableUsers[1].id)
          .then(isAdmin => {
            expect(isAdmin).toEqual(true);
            done();
          });
      });
  });

  it('remove a user as admin', (done) => {
    client.removeAdminRoleForUser(availableUsers[1].id)
      .then(() => {
        client.isUserAdmin(availableUsers[1].id)
          .then(isAdmin => {
            expect(isAdmin).toEqual(false);
            done();
          });
      });
  });

  var createdUser;

  it('adds a user with role but not admin', (done) => {
    client.createUser('blabla@new.fr', {
      email: 'blabla@new.fr',
      firstName: 'Jean',
      lastName: 'Claude'
    }, '1234', ['guest'])
      .then(resp => client.getUserByUsername('blabla@new.fr')
      .then(user => {
        createdUser = user;
        return client.isUserAdmin(createdUser.id)
      })
      .then(isAdmin => {
        expect(isAdmin).toEqual(false);
        return client.getRolesForRealm(createdUser.id)
      })
      .then(roles => {
        expect(roles.find(role => role.name == 'guest')).toBeTruthy();
        done();
      })
      .catch(err => {
        console.log(err);
        done();
      }));
  });


  it('removes a user', (done) => {
    client.removeUser(createdUser.id)
      .then(resp => {
        return client.getUserByUsername('blabla@new.fr')
      })
      .then(user => {
        fail('Should not pass here');
        done();
      })
      .catch(err => done());
  });

  var createdUser;

  it('adds a user with admin creadentials', (done) => {
    client.createUser('blabla@new.fr', {
        email: 'blabla@new.fr',
        firstName: 'Jean',
        lastName: 'Claude'
      }, '1234', null, true)
      .then(resp => client.getUserByUsername('blabla@new.fr')
        .then(user => {
          createdUser = user;
          return client.isUserAdmin(createdUser.id)
        })
        .then(isAdmin => {
          expect(isAdmin).toEqual(true);
          done();
          //return client.removeUser(createdUser.id)
        })
        .then(() => done())
        .catch(err => {
          console.log(err);
          done();
        }));
  });
});