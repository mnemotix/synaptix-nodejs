/**
 * This file is part of the synaptix-nodejs package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 31/05/2016
 */

import request from 'request';
import format from 'string-template';
import winston from 'winston-color';

//request.debug = true;

const KC_REST_ENDPOINT = {
  users : '/users',
  user : '/users/{userId}',
  identityProvider : '/identity-provider/instances/{alias}',
  userResetPassword : '/users/{userId}/reset-password',
  userResetPasswordByMail : '/users/{userId}/execute-actions-email',
  userFederatedIdentity : '/users/{userId}/federated-identity/{identityProvider}',
  groups: '/groups',
  realmRolesMapping : '/users/{userId}/role-mappings/realm',
  realmRolesMappingAvailable : '/users/{userId}/role-mappings/realm/available',
  realmClientRolesMapping : '/users/{userId}/role-mappings/clients/{clientUid}',
  realmClientRolesMappingAvailable : '/users/{userId}/role-mappings/clients/{clientUid}/available',
  clients : '/clients'
};

export default class KeycloakClient {
  /**
   *
   * @param login
   * @param password
   * @return {Session}
   */
  getAdminSession(login, password) {
    return new Session({
      realm: "master",
      apiRealm: process.env.OAUTH_REALM || 'synaptix',
      clientId: "admin-cli",
      login, password
    });
  }

  /**
   * @param accessToken
   * @param clientId
   * @return {Session}
   */
  getAccessTokenSession(accessToken, clientId) {
    return new Session({
      accessToken,
      clientId,
      realm: process.env.OAUTH_REALM || 'synaptix',
      apiRealm: process.env.OAUTH_REALM || 'synaptix'
    });
  }
}

class Session{
  authorization;

  constructor({oauthHost, clientId, accessToken, login, password, realm, apiRealm}){
    this.accessToken = accessToken;

    if (login && password){
      this.login = login;
      this.password = password;
    }

    let host = oauthHost || process.env.OAUTH_HOST || process.env.OAUTH_REDIRECT_URL;

    this.baseAPIUrl = `${host}/auth/admin/realms/${apiRealm}`;
    this.baseUrl = `${host}/auth/realms/${realm}`;
    this.clientId  = clientId;
  }

  requestToken(username, password) {
    return new Promise((resolve, reject) => {
      request({
        uri: `${this.baseUrl}/protocol/openid-connect/token`,
        json: true, // Automatically parses the JSON string in the response
        method: 'POST',
        form: {
          username: username,
          password: password,
          client_id: this.clientId,
          grant_type: 'password'
        },
        followRedirect: true,
        followAllRedirects: true,
        followOriginalHttpMethod: true
      }, (error, response, body) => {
        if (!error) {
          resolve(body);
        } else {
          reject(error);
        }
      })
    });
  }

  async request(uri, options = {}){
    if (!this.accessToken && this.login && this.password) {
      let {access_token: accessToken} = await this.requestToken(this.login, this.password);
      this.accessToken = accessToken;
    }

    return new Promise((resolve, reject) => {
      request({
        uri: `${this.baseAPIUrl}${uri}`,
        headers: {
          'Authorization': `Bearer ${this.accessToken}`
        },
        followRedirect: true,
        followAllRedirects: true,
        followOriginalHttpMethod: true,
        json: true,
        ...options
      }, (error, response, body) => {
        if (!error) {
          resolve(body, response);
        } else {
          winston.error(error, response);
          reject(error);
        }
      });
    });
  }

  getIdentityProvider(alias){
    return this.request(format(KC_REST_ENDPOINT.identityProvider, {alias}));
  }

  getUsers(first, max){
    return this.request(KC_REST_ENDPOINT.users, {
      qs : {
        first, max
      }
    });
  }

  getUser(userId){
    return this.request(format(KC_REST_ENDPOINT.user, {
      userId
    }));
  }

  async getUserByUsername(username) {
    let users = await this.request(KC_REST_ENDPOINT.users, {
      qs: {username}
    });

    if(users && users.length === 1){
      return users[0];
    } else {
      throw 'User not found';
    }

  }

  async createUser(username, props, password, roles = null, isAdmin = false, temporaryPassword = true){

    let rsp =  await this.request(KC_REST_ENDPOINT.users, {
      method: 'POST',
      body : {
        username,
        ...props,
        enabled: true
      }
    });

    let createdUser = await this.getUserByUsername(username);

    if(isAdmin) {
      await this.setAdminRoleToUser(createdUser.id);
    }

    if(roles) {
      await this.setRealmRoleToUser(createdUser.id, roles);
    }

    await this.request(format(KC_REST_ENDPOINT.userResetPassword, {
      userId : createdUser.id
    }), {
      method: 'PUT',
      body : {
        temporary: temporaryPassword,
        type: "password",
        value: password
      }
    });

    return createdUser;
  }

  async createUserWithFederatedIdentity(username, props, identityProvider, userId, userName, password){
    let rsp =  await this.request(KC_REST_ENDPOINT.users, {
      method: 'POST',
      body : {
        username,
        ...props,
        enabled: true
      }
    });

    let createdUser = await this.getUserByUsername(username);

    await this.request(format(KC_REST_ENDPOINT.userFederatedIdentity, {
      userId : createdUser.id,
      identityProvider
    }), {
      method: 'POST',
      body : {
        identityProvider,
        userId,
        userName
      }
    });

    await this.request(format(KC_REST_ENDPOINT.userResetPassword, {
      userId : createdUser.id
    }), {
      method: 'PUT',
      body : {
        temporary: false,
        type: "password",
        value: password
      }
    });

    return createdUser;
  }

  resetUserPassword(userId, password){
    return this.request(format(KC_REST_ENDPOINT.userResetPassword, {
      userId
    }), {
      method: 'PUT',
      body : {
        temporary: true,
        type: "password",
        value: password
      }
    });
  }

  resetUserPasswordByMail(userId){
    return this.request(format(KC_REST_ENDPOINT.userResetPasswordByMail, {
      userId
    }), {
      method: 'PUT',
      body : ["UPDATE_PASSWORD"]
    });
  }

  removeUser(userId) {
    return this.request(format(KC_REST_ENDPOINT.user, {
      userId
    }), {
      method: 'DELETE'
    });
  }

  getClients(){
    if (!this.clients) {
      return this.request(KC_REST_ENDPOINT.clients)
        .then(clients => {
          this.clients = clients;
          return clients;
        })
    } else {
      return Promise.resolve(this.clients);
    }
  }

  getClientUid(clientId){
    return this.getClients()
      .then(clients => {
        let client = clients.find(client => client.clientId === clientId);

        if(client) {
          return client.id;
        }
      })
  }

  isUserAdmin(userId) {
    return this.getClientUid('realm-management')
      .then(clientUid => {
        return this.request(format(KC_REST_ENDPOINT.realmClientRolesMapping, {
          userId,
          clientUid
        }))
      })
      .then(roles => {
         return !!roles.find(role => role.name === 'realm-admin');
      });
  }

  getAvailableRolesForClient(userId, clientId) {
    return this.getClientUid(clientId)
      .then((clientUid) => this.request(
        format(KC_REST_ENDPOINT.realmClientRolesMappingAvailable, {
          userId,
          clientUid
        })
      ));
  }

  getRolesForClient(userId, clientId) {
    return this.getClientUid(clientId)
      .then((clientUid) => this.request(
        format(KC_REST_ENDPOINT.realmClientRolesMapping, {
          userId,
          clientUid
        })
      ));
  }

  getAvailableRolesForRealm(userId) {
    return this.request(
      format(KC_REST_ENDPOINT.realmRolesMappingAvailable, {
        userId
      })
    );
  }

  getRolesForRealm(userId) {
    return this.request(
      format(KC_REST_ENDPOINT.realmRolesMapping, {
        userId
      })
    );
  }

  setRealmRoleToUser(userId, roleName) {
    return this.updateRealmRoleToUser(userId, roleName);
  }

  removeRealmRoleForUser(userId, roleName) {
    return this.updateRealmRoleToUser(userId, roleName, true);
  }

  updateRealmRoleToUser(userId, roleName, removeIt = false) {
    return this[removeIt ? 'getRolesForRealm' : 'getAvailableRolesForRealm'](userId)
      .then(roles => {
        let realmRole = roles.find(role => role.name === roleName);

        if(realmRole) {
          return this.request(
            format(KC_REST_ENDPOINT.realmRolesMapping, {
              userId
            }),
            {
              method: removeIt ? 'DELETE' : 'POST',
              body: [].concat( realmRole )
            }
          );
        } else {
          throw Error(`Role ${roleName} doesn't exists for this user, or already set`);
        }
      });
  }

  updateAdminRoleToUser(userId, setAsAdmin = true) {
    return this[setAsAdmin ? 'getAvailableRolesForClient' : 'getRolesForClient'](userId, 'realm-management')
      .then(roles => {
        let realmAdminRole = roles.find(role => role.name === 'realm-admin');

        if (realmAdminRole) {
          return this.getClientUid('realm-management')
            .then(clientUid => this.request(
              format(KC_REST_ENDPOINT.realmClientRolesMapping, {
                userId,
                clientUid
              }),
              {
                method: setAsAdmin ? 'POST' : 'DELETE',
                body: [].concat(realmAdminRole)
              }
            ));
        }
      });
  }

  setAdminRoleToUser(userId) {
    return this.updateAdminRoleToUser(userId, true);
  }

  removeAdminRoleForUser(userId) {
    return this.updateAdminRoleToUser(userId, false);
  }
}