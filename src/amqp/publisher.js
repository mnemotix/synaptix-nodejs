import uuid from 'uuid/v4';
import cbStore from 'callback-store';
import createConnection from './create-connection';

export default async (opt = {}) => {
  const {publisherName, ttl} = opt;
  const {connection, exchange} = await createConnection(opt);

  const resultsQueueName = `${process.env.UUID || uuid()}_pid:${process.pid}_publisher:${publisherName}_callback_queue`;

  const callbacks = cbStore();

  const exchangeOptions =  {
    exclusive: false,
    autoDelete: true,
    ...opt.exchangeOptions
  };

  function onResult (message, headers, deliveryInfo) {
    const cb = callbacks.get(deliveryInfo.correlationId);
    if (!cb) return;

    const args = [].concat(message);

    cb.apply(null, args);
  }

  /**
   * call a remote command
   *
   * @param {string} cmd   command name
   * @param {Buffer|Object|String}params    parameters of command
   * @param {function} cb        callback
   * @param {object} options   advanced options of amqp
   */
  function call(cmd, params, cb, options) {
    options = options || {};
    options.contentType = 'application/json';

    if (!cb) {
      exchange.publish(cmd, params, options);
      return
    }

    const corrId = uuid();
    callbacks.add(corrId, cb, ttl || 5e3);

    options.mandatory = true;
    options.replyTo = resultsQueueName;
    options.correlationId = corrId;

    exchange.publish(cmd, params, options, err => {
      if (err) {
        const cb = callbacks.get(corrId);
        cb(err);
      }
    })
  }

  return new Promise((resolve, reject) => {
    connection.queue(
      resultsQueueName,
      exchangeOptions,
      queue => {
        queue.subscribe(onResult);
        queue.bind(exchange, resultsQueueName);
        resolve({call});
      }
    );
  });
}
