import amqp from 'amqp';
import winston from 'winston-color';

let connect = function(options, cb){
  return new Promise((resolve, reject) => {
    if (!options.uri && !options.host) {
      options.uri = 'amqp://guest:guest@localhost:5672'
    }

    let attempts = 1;

    const connection = amqp.createConnection(options);

    connection.on('ready', () => {
      attempts = 1;

      connection.exchange(options.exchangeName, {
        autoDelete: false,
      }, exchange => {
        resolve({
          exchange,
          connection
        })
      })
    });

    connection.on('error', (e) => {
      winston.error("AMQP broker error:", e);

      if (attempts < 5) {
        winston.info(`Attempt reconnection in ${attempts} seconds...`);
        setTimeout(() => {
          attempts++;
          connection.reconnect();
        }, 1000 * attempts)
      }
    });
  });
};

export default (options, cb) => {
  return connect(options, cb);
}
