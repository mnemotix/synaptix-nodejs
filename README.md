NodeJS publishers for synaptix modules
--------------------------------------


# Installation

```bash
npm install
```


## Run integration tests

With default environment variables :

```bash
npm run-script test-integration
```

Otherwise :

```bash
RABBITMQ_HOST=192.168.1.25 RABBITMQ_LOGIN=mnxuser RABBITMQ_PASSWORD=mnxpowa! npm run-script test-integration
```

With :

 - ``RABBITMQ_HOST`` RabbitMQ host 
 - ``RABBITMQ_LOGIN`` RabbitMQ login 
 - ``RABBITMQ_PASSWORD`` RabbitMQ password
 - ``RABBITMQ_EXCHANGE_NAME`` RabbitMQ exchange name





