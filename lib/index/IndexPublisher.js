'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   * This file is part of the synaptix-nodejs package.
                                                                                                                                                                                                                                                                   *
                                                                                                                                                                                                                                                                   * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
                                                                                                                                                                                                                                                                   *
                                                                                                                                                                                                                                                                   * Date : 28/04/2016
                                                                                                                                                                                                                                                                   */

/**
 * Class used to publish commands to the Index module on Synaptix.
 *
 *  @extends AbstractPublisher
 */


var _AbstractPublisher = require('../AbstractPublisher');

var _AbstractPublisher2 = _interopRequireDefault(_AbstractPublisher);

var _requestPromiseNative = require('request-promise-native');

var _requestPromiseNative2 = _interopRequireDefault(_requestPromiseNative);

var _winstonColor = require('winston-color');

var _winstonColor2 = _interopRequireDefault(_winstonColor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//request.debug = true;

class IndexPublisher extends _AbstractPublisher2.default {
  getName() {
    return 'index';
  }

  /**
   * Search for nodes
   *
   * @param {string|string[]} docTypes Document index types to match.
   * @param {object} body ES query
   * @param {int} from
   * @param {int} size
   * @param fullResponse
   *
   * @param sourcePropName
   * @deprecated Use this::query instead
   *
   * @returns {Promise.<Node[],Error>}
   */
  search(docTypes, body, from = 0, size = 20, fullResponse = false, sourcePropName = "source") {
    return this.query({
      docTypes,
      query: body,
      from,
      size,
      fullResponse,
      sourcePropName
    });
  }

  /**
   * Ask index for something
   *
   * @param args
   */
  query(args) {
    let { docTypes, query, from, size, fullResponse, sourcePropName } = args;

    if (typeof docTypes === 'string') {
      docTypes = [docTypes];
    }

    docTypes = docTypes.map(docType => docType.toLowerCase());

    let extra = {};

    if (query.sort) {
      for (let fieldName in query.sort) {
        extra.sortCriteria = {
          fieldName,
          desc: query.sort[fieldName].order === 'desc'
        };
      }
    }

    // let date = (new Date()).getMilliseconds();
    // winston.profile(date);
    if (process.env.ES_URL && process.env.ES_INDEX_NAME && query) {
      // winston.debug(`Direct ES requesting`, `${process.env.ES_URL}/${process.env.ES_INDEX_NAME}/${docTypes.join(',')}/_search`, JSON.stringify(query, null, ' '));

      return (0, _requestPromiseNative2.default)({
        uri: `${process.env.ES_URL}/${process.env.ES_INDEX_NAME}/${docTypes.join(',')}/_search`,
        method: 'POST',
        json: true,
        body: query,
        headers: {
          'content-type': 'application/json'
        }
      }).then(result => {
        return fullResponse ? result : result.hits;
      }).catch(error => {
        _winstonColor2.default.error(`Resp error to ES requesting ${process.env.ES_URL}/${process.env.ES_INDEX_NAME}/${docTypes.join(',')}/_search`, JSON.stringify(query, null, ' '));
        _winstonColor2.default.error(JSON.stringify(error, null, ' '));
      });
    } else {
      return this.publish('index.search', _extends({
        docTypes,
        [sourcePropName || 'source']: query,
        per_page: size,
        offset: from
      }, extra)).then(result => {
        return fullResponse ? result : result.hits;
      });
    }
  }

  /**
   * Percolate against document
   *
   * @param {string} docType Document index type to percolate.
   * @param {string} docId Document id against to percolate
   * @param {string} parentId Parent document if the relation is parent/child
   *
   * @returns {Promise.<Node[],Error>}
   */
  percolate(docType, docId, parentId) {
    return this.publish('index.percolate', {
      docType: docType.toLowerCase(),
      docId,
      parentId
    });
  }
}
exports.default = IndexPublisher;