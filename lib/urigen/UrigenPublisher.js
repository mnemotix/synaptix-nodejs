'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _AbstractPublisher = require('../AbstractPublisher');

var _AbstractPublisher2 = _interopRequireDefault(_AbstractPublisher);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Class used to publish commands to the URIGen module on Synaptix.
 *
 * @extends AbstractPublisher
 */
class UrigenPublisher extends _AbstractPublisher2.default {

  getName() {
    return 'urigen';
  }

  /**
   * Get an URI for a given id and object type.
   *
   * @param {string} objectType object type (skos:concept, foaf:person...)
   * @param {string} id id of the object
   * @returns {Promise.<string, Error>} A promise that returns an URI
   */
  getUri(objectType, id) {
    return this.publish('uri.get', `${objectType}:${id}:uri`);
  }

  /**
   * Create an URI given an object type
   *
   * @param {string} objectType object type (skos:concept, foaf:person...)
   * @returns {Promise.<string, Error>} A promise that returns an URI
   */
  createUri(objectType) {
    return this.publish('uri.create', {
      objectType
    });
  }

  /**
   *
   * @param {string} objectType object type (skos:concept, foaf:person...)
   * @param {string} id id of the object
   * @returns {Promise.<bool, Error>} A promise that returns True if deleted, False otherwise
   */
  deleteUri(objectType, id) {
    return this.publish('uri.delete', `${objectType}:${id}:*`);
  }
}
exports.default = UrigenPublisher; /**
                                    * This file is part of the Koncept package.
                                    *
                                    * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
                                    *
                                    * Date : 13/04/2016
                                    */