'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _callback_api = require('amqplib/callback_api');

var _callback_api2 = _interopRequireDefault(_callback_api);

var _winstonColor = require('winston-color');

var _winstonColor2 = _interopRequireDefault(_winstonColor);

var _v = require('uuid/v4');

var _v2 = _interopRequireDefault(_v);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = class {

  getName() {
    return (0, _v2.default)();
  }

  constructor() {
    this.amqpURI = `amqp://${process.env.RABBITMQ_LOGIN}:${process.env.RABBITMQ_PASSWORD}@${process.env.RABBITMQ_HOST}`;
    this.exchangeName = process.env.RABBITMQ_EXCHANGE_NAME || 'synaptix-local';
    this.routingKeys = [];
    this.callbacks = [];

    this.connect();
  }

  connect() {
    _callback_api2.default.connect(this.amqpURI, (err, conn) => {

      if (!err) {
        conn.createChannel((err, ch) => {
          ch.assertExchange(this.exchangeName, 'topic', { durable: false });

          const resultsQueueName = `${process.env.UUID || (0, _v2.default)()}_pid:${process.pid}_consumer:${this.getName()}_queue`;

          ch.assertQueue(resultsQueueName, { exclusive: true }, (err, q) => {
            _winstonColor2.default.info(`AMQP connection OK. Waiting AMQP messages on exchange : amqp://${this.exchangeName}/${q.queue}.`);

            this.getRoutingKeys().map(routingKey => ch.bindQueue(q.queue, this.exchangeName, routingKey));

            ch.consume(q.queue, this.onMessage.bind(this), { noAck: true });
          });
        });

        conn.on("error", err => this.onError(err, true));
      } else {
        this.onError(err, true);
      }
    });
  }

  onError(err, displayWarning = false) {
    if (displayWarning) {
      _winstonColor2.default.warn(`AMQP connection lost ! Attempt reconnection every 5s...`, err);
    }

    setTimeout(this.connect.bind(this), 5000);
  }

  onMessage(msg) {
    throw "You must override this method. This is the callback after having received a message.";
  }

  getRoutingKeys() {
    throw "You must override this method. This provides routing keys to select topics to listen to.";
  }

  dispatch(command, ...params) {
    if (this.callbacks[command]) {
      this.callbacks[command].map(callback => callback(...params));
    }
  }

  dispatchAfterDelay(delay, command, ...params) {
    if (this.callbacks[command]) {
      this.callbacks[command].map(callback => {
        setTimeout(() => {
          callback(...params);
        }, delay);
      });
    }
  }

  bind(command, callback) {
    if (!this.callbacks[command]) {
      this.callbacks[command] = [];
    }

    this.callbacks[command].push(callback);
  }
}; /**
    * This file is part of the Koncept package.
    *
    * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
    *
    * Date : 13/04/2016
    */