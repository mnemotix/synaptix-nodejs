'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * This file is part of the synaptix-nodejs package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 15/04/2016
 */

exports.default = class {
  /** @type {string} node id */
  constructor(id, properties, meta) {
    this.id = id;
    this.properties = properties;
    this.meta = meta;
  }
  /** @type {list} node properties */


  static getOrigin() {
    return process.env.GRAPH_ORIGIN || 'mnx:app:nodejs';
  }

  getId() {
    return this.id;
  }

  getProperties() {
    return this.properties;
  }

  getMeta() {
    return this.meta;
  }

  /**
   * Get a property value
   * @param {string} name
   * @returns {*}
   */
  getPropertyValue(name) {
    if (this.properties) {
      return this.properties[name];
    }
  }

  /**
   * Get a meta value
   * @param {string} name
   * @returns {*}
   */
  getMetaValue(name) {
    if (this.meta) {
      return this.meta[name];
    }
  }
};