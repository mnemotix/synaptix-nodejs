'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   * This file is part of the synaptix-nodejs package.
                                                                                                                                                                                                                                                                   *
                                                                                                                                                                                                                                                                   * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
                                                                                                                                                                                                                                                                   *
                                                                                                                                                                                                                                                                   * Date : 15/04/2016
                                                                                                                                                                                                                                                                   */

var _Abstract = require('./Abstract');

var _Abstract2 = _interopRequireDefault(_Abstract);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class Node extends _Abstract2.default {

  /**
   * @constructor
   *
   * @param nodeType
   * @param id
   * @param properties
   * @param meta
   * @param uri
   */

  /** @type {string} node type */
  constructor(nodeType, id, properties, meta, uri) {
    super(id, properties, meta);

    this.nodeType = nodeType;
    this.uri = uri;
  }
  /** @type {string} uri */


  getNodeType() {
    return this.nodeType;
  }

  /**
   * Get URI
   */
  getUri() {
    return this.uri;
  }

  /**
   * Serialize Node into a JSON compatible format.
   *
   * @returns {{nodeType: string, id: string, properties: list}}
   */
  serialize() {
    return {
      nodeType: this.nodeType,
      _id: this.id,
      _origin: _Abstract2.default.getOrigin(),
      _source: _extends({}, this.properties),
      uri: this.uri
    };
  }

  /**
   * Factory to create a node
   *
   * @param nodeType
   * @param id
   * @param properties
   * @param meta
   * @param uri
   * @returns {Node}
   */
  static createNode(nodeType, id, properties, meta, uri) {
    return new Node(nodeType, id, properties, meta, uri);
  }

  /**
   * Factory to create a partial node serialization without id
   *
   * @param {string} nodeType Node type
   * @param {string} uri Node URI
   * @param {object} properties Additionals node properties to store.
   * @param {object} meta Additionals meta to consolidate
   */
  static serializePartial(nodeType, uri, properties = {}, meta = null) {
    if (typeof properties._enabled === 'undefined') {
      properties._enabled = true;
    }

    let _meta = meta ? { _meta: meta } : {};

    return _extends({
      nodeType: Node.normalizedNodeType(nodeType),
      uri,
      _origin: _Abstract2.default.getOrigin(),
      _source: properties
    }, _meta);
  }

  /**
   * Factory to create a node serialization
   *
   * @param {string} nodeType Node type
   * @param {string} id Node id
   * @param {string} uri Node URI
   * @param {object} properties Additionals node properties to store.
   * @param {object} meta Additionals meta to consolidate
   */
  static serializeNode(nodeType, id, uri, properties = {}, meta = null) {
    let _meta = meta ? { _meta: meta } : {};

    return _extends({
      nodeType: Node.normalizedNodeType(nodeType),
      _id: id,
      _origin: _Abstract2.default.getOrigin(),
      _source: properties,
      uri
    }, _meta);
  }

  static normalizedNodeType(nodeType) {
    return nodeType.charAt(0).toUpperCase() + nodeType.slice(1);
  }
}
exports.default = Node;