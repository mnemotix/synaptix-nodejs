'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createNode = exports.createEdge = exports.Filter = exports.Edge = exports.Node = undefined;

var _Node = require('./Node');

var _Node2 = _interopRequireDefault(_Node);

var _Edge = require('./Edge');

var _Edge2 = _interopRequireDefault(_Edge);

var _Filter = require('./Filter');

var _Filter2 = _interopRequireDefault(_Filter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createEdge(subj, pred, obj, properties) {
  return new _Edge2.default(subj, pred, obj, properties);
} /**
   * This file is part of the synaptix-nodejs package.
   *
   * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
   *
   * Date : 15/04/2016
   */

function createNode(nodeType, id, properties, meta, uri) {
  return new _Node2.default(nodeType, id, properties, meta, uri);
}

exports.Node = _Node2.default;
exports.Edge = _Edge2.default;
exports.Filter = _Filter2.default;
exports.createEdge = createEdge;
exports.createNode = createNode;