'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Abstract = require('./Abstract');

var _Abstract2 = _interopRequireDefault(_Abstract);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class Edge extends _Abstract2.default {

  /**
   * @constructor
   *
   * @param subj
   * @param pred
   * @param obj
   * @param properties
   */

  /** @type {string} node subject nodeType */

  /** @type {string} edge label */
  constructor(id, subj, pred, obj, properties) {
    super(id, properties);

    this.subj = subj;
    this.pred = pred;
    this.obj = obj;
  }
  /** @type {string} node object nodeType */

  /** @type {string} node object id (@in direction) */

  /** @type {string} node subject id (@out direction) */


  getSubj() {
    return this.subj;
  }

  getPred() {
    return this.pred;
  }

  getObj() {
    return this.obj;
  }

  getSubjNodeType() {
    return this.subjNodeType;
  }

  getObjNodeType() {
    return this.objNodeType;
  }

  getProperties() {
    return this.properties;
  }

  setSubjNodeType(value) {
    this.subjNodeType = value;
  }

  setObjNodeType(value) {
    this.objNodeType = value;
  }

  /**
   * Serialize Edge into a JSON compatible format.
   *
   * @returns {{subj: string, pred: string, obj: string, properties: list}}
   */
  serialize() {
    return {
      subj: this.subj,
      pred: this.pred,
      obj: this.obj,
      _origin: _Abstract2.default.getOrigin(),
      _source: this.properties
    };
  }

  /**
   * Normalize the predicate name
   * @param edge
   */
  static normalizePredicate(edge) {
    return edge.toUpperCase();
  }

  /**
   * Get an edge type structure in command
   *
   * @param {string} subj Subject node id (edge OUT direction)
   * @param {string} pred Relation name
   * @param {string} obj  Object node id (edge IN direction)
   * @param properties
   *
   * @returns {{subj: *, pred: *, obj: *}}
   */
  static serializePartial(subj, pred, obj, properties = {}) {
    if (typeof properties._enabled === 'undefined') {
      properties._enabled = true;
    }

    return {
      subj,
      pred: Edge.normalizePredicate(pred),
      obj,
      _origin: _Abstract2.default.getOrigin(),
      _source: properties
    };
  }
}
exports.default = Edge; /**
                         * This file is part of the synaptix-nodejs package.
                         *
                         * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
                         *
                         * Date : 15/04/2016
                         */