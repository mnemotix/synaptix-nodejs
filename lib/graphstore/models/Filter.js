"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

/**
 * This file is part of the synaptix-nodejs package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 15/04/2016
 */

exports.default = class {

  /**
   * @constructor
   *
   * @param id
   * @param nodeLabels
   * @param inDirection
   * @param edgeLabels
   * @param nodeAttributes
   * @param size
   * @param from
   */

  /** @type {number} pagination size */

  /** @type {list} edge labels to filter on */

  /** @type {list} node label */
  constructor(id, nodeLabels, inDirection, edgeLabels, nodeAttributes = [], size = null, from = 0) {
    this.inDirection = true;

    this.id = id;
    this.nodeLabels = nodeLabels;
    this.inDirection = inDirection;
    this.edgeLabels = edgeLabels;
    this.nodeAttributes = nodeAttributes;
    this.size = size;
    this.from = from;
  }
  /** @type {number} pagination offset */

  /** @type {list} node attributes to filter on */

  /** @type {boolean} Is IN direction ? OUT otherwise. */

  /** @type {string} id */


  serialize() {
    var pagination = this.size && this.from ? {
      size: this.size,
      from: this.from
    } : {};

    return _extends({
      id: this.id,
      nodeLabels: this.nodeLabels || [],
      inDirection: this.inDirection,
      edgeLabels: this.edgeLabels || [],
      nodeAttributes: this.nodeAttributes || []
    }, pagination);
  }
};