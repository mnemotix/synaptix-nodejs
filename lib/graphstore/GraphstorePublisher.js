'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _AbstractPublisher = require('../AbstractPublisher');

var _AbstractPublisher2 = _interopRequireDefault(_AbstractPublisher);

var _models = require('./models');

var _winstonColor = require('winston-color');

var _winstonColor2 = _interopRequireDefault(_winstonColor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * This file is part of the Koncept package.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Date : 13/04/2016
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            */

/**
 * Class used to publish commands to the GraphStore module on Synaptix.
 *
 *  @extends AbstractPublisher
 */

class GraphstorePublisher extends _AbstractPublisher2.default {
  getName() {
    return 'graphstore-dse';
  }

  getEnabledNodeFilter() {
    return "has('_enabled', true)";
  }

  /**
   * Create a node
   *
   * @param {string} nodeType Node type (skos:concept, foaf:person...)
   * @param {string} uri Node URI
   * @param {object} properties Additionals node properties to store.
   * @param {object} meta Additionals node metas to store.
   * @param {object} context Context information to add as AMQP headers.
   *
   * @example
   * Create a skos:concept with { uri : "mnx:concept:1", historyNote: "Blabla" }
   *
   * {
   *   "nodeType": "skos:concept",
   *   "properties" : [{
   *    "label" : "uri",
   *    "value: "mnx:concept:1",
   *   },{
   *    "label" : "historyNote",
   *    "value" : "Blabla"
   *   }]
   * }
   *
   * @returns {Promise.<Node, Error>} A promise that returns a node object
   */
  createNode(nodeType, uri, properties = {}, meta = null, context = undefined) {
    if (typeof properties._enabled === 'undefined') {
      properties._enabled = true;
    }

    return this.publish('node.create', _models.Node.serializePartial(nodeType, uri, properties, meta), context).then(node => (0, _models.createNode)(node.nodeType, node._id, node._source, node._meta, null, node.uri));
  }

  /**
   * Get a node by id
   *
   * @param {string} id Node id
   * @param {object} context Context information to add as AMQP headers.
   * @param {boolean} skipDisabled skip node if disabled
   *
   * @returns {Promise.<Node, Error>} A promise that returns a node object
   */
  getNode(id, context, skipDisabled = true) {
    var _this = this;

    return _asyncToGenerator(function* () {
      let query = `g.V('${id}')${skipDisabled ? '.' + _this.getEnabledNodeFilter() : ''}`;

      let nodes = yield _this.queryGraphNodes(query, context);

      if (nodes.length === 1) {
        return nodes[0];
      } else {
        throw new Error(`Node ${id} not found`);
      }
    })();
  }

  /**
   * Get a list of nodes given a label
   *
   * @param {string|list} labelTypes A label or a list of label type.
   * @param from
   * @param size
   * @param {object} context Context information to add as AMQP headers.
   * @param {boolean} skipDisabled skip node if disabled
   * @returns {Promise.<Node[], Error>} A promise that returns a list of node object
   */
  getNodesWithLabel(labelTypes, from = 0, size = null, context, skipDisabled = true) {
    var _this2 = this;

    return _asyncToGenerator(function* () {
      let query = `g.V().hasLabel(${JSON.stringify(labelTypes)})${skipDisabled ? '.' + _this2.getEnabledNodeFilter() : ''}`;

      return _this2.queryGraphNodes(query, context);
    })();
  }

  /**
   * Attempt a query on a graph and return nodes
   *
   * @param {object} gremlinQuery Query on Gremlin format
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Promise.<Node[], Error>} A promise that returns a list of node object
   */
  queryGraphNodes(gremlinQuery, context) {
    var _this3 = this;

    return _asyncToGenerator(function* () {
      let nodes = yield _this3.publish('nodes.select', {
        query: gremlinQuery.replace(/[\n\r]/g, '')
      }, context);

      return nodes.map(function (node) {
        return (0, _models.createNode)(node.nodeType, node._id, node._source, null, node.uri);
      });
    })();
  }

  /**
   * Attempt a query on a graph and return edges
   *
   * @param {object} gremlinQuery Query on Gremlin format
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Promise.<Edge[], Error>} A promise that returns a list of node object
   */
  queryGraphEdges(gremlinQuery, context) {
    var _this4 = this;

    return _asyncToGenerator(function* () {
      let edges = yield _this4.publish('edges.select', {
        query: gremlinQuery.replace(/[\n\r]/g, '')
      }, context);

      return edges.map(function (edge) {
        return (0, _models.createEdge)(edge._id, edge.subj, edge.pred, edge.obj, edge._source);
      });
    })();
  }

  /**
   * Attempt a query on a graph
   *
   * @param {object} gremlinQuery Query on Gremlin format
   * @param {object} context Context information to add as AMQP headers.
   *
   * @deprecated use queryRawGraph
   * @alias queryRawGraph
   * @returns {Promise.<Array, Error>} A promise that returns a raw gremlin response.
   */
  queryGraph(gremlinQuery, context) {
    return this.queryRawGraph(gremlinQuery, context);
  }

  /**
   * Attempt a query on a graph
   *
   * @param {object} gremlinQuery Query on Gremlin format
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Promise.<Array, Error>} A promise that returns a raw gremlin response.
   */
  queryRawGraph(gremlinQuery, context) {
    return this.publish('graph.query', {
      query: gremlinQuery.replace(/\n/, '')
    }, context);
  }

  /**
   * Select a subgraph
   *
   * @param {object} gremlinQuery Query on Gremlin format
   * @param {object} context Context information to add as AMQP headers.
   * @param {boolean} nodesAsArray Return nodes as array or leave it as object
   *
   * @returns {Promise.<Node[], Error>} A promise that returns a graph object composed of
   * {
   *  nodes: [Node],
   *  edges: [Edge]
   * }
   */
  selectSubGraph(gremlinQuery, context, nodesAsArray = true) {
    return this.publish('graph.select', {
      query: gremlinQuery.replace(/\n/, '')
    }, context).then(graphes => {
      let graph = graphes[0];

      let nodes = nodesAsArray ? [] : {};

      Object.keys(graph.nodes).map(nodeKey => {
        let node = graph.nodes[nodeKey];

        let properties = {};

        Object.keys(node._source).map(key => {
          if (Array.isArray(node._source[key])) {
            properties[key] = node._source[key][0].value;
          } else {
            properties[key] = node._source[key];
          }
        });

        node = (0, _models.createNode)(node.nodeType, node._id, properties, null, node.uri);

        if (nodesAsArray) {
          nodes.push(node);
        } else {
          nodes[nodeKey] = node;
        }
      });

      return {
        nodes,
        edges: graph.edges.map(edge => {
          let edgeObject = (0, _models.createEdge)(edge._id, edge.subj, edge.pred, edge.obj, edge._source);

          let objNode = nodes.find(node => node.getId() === edge.obj);
          let subjNode = nodes.find(node => node.getId() === edge.subj);

          edgeObject.setSubjNodeType(subjNode.getNodeType());
          edgeObject.setObjNodeType(objNode.getNodeType());

          return edgeObject;
        })
      };
    });
  }

  /**
   * Get nodes having an incoming relation with following node.
   *
   * @param {string} id Node id
   * @param {string} edgeLabel edge label
   * @param from
   * @param size
   * @param {object} context Context information to add as AMQP headers.
   * @param {boolean} skipDisabled skip node if disabled
   * @returns {Promise.<Node[], Error>} A promise that returns a list of node object
   */
  getNodesWithInRelationTo(id, edgeLabel, from = 0, size = null, context, skipDisabled = true) {
    let query = `g.V('${id}').in('${edgeLabel}')${skipDisabled ? '.' + this.getEnabledNodeFilter() : ''}`;

    if (size) {
      if (from > 0) {
        query += `.range(${from}, ${size})`;
      } else {
        query += `.limit(${size})`;
      }
    }

    return this.queryGraphNodes(query, context);
  }

  /**
   * Get nodes having an outcoming relation with following node.
   *
   * @param {string} id Node id
   * @param {string} edgeLabel edge label
   * @param from
   * @param size
   * @param {object} context Context information to add as AMQP headers.
   * @param {boolean} skipDisabled skip node if disabled
   * @returns {Promise.<Node[], Error>} A promise that returns a list of node object
   */
  getNodesWithOutRelationTo(id, edgeLabel, from = 0, size = null, context, skipDisabled = true) {
    let query = `g.V('${id}').out('${edgeLabel}')${skipDisabled ? '.' + this.getEnabledNodeFilter() : ''}`;

    if (size) {
      if (from > 0) {
        query += `.range(${from}, ${size})`;
      } else {
        query += `.limit(${size})`;
      }
    }

    return this.queryGraphNodes(query, context);
  }

  /**
   * Get a node bu URI
   *
   * @param {string} uri Node URI
   * @param {object} context Context information to add as AMQP headers.
   * @param {boolean} skipDisabled skip node if disabled
   * @returns {Promise.<Node, Error>} A promise that returns a node object
   */
  getNodeByURI(uri, context, skipDisabled = true) {
    var _this5 = this;

    return _asyncToGenerator(function* () {
      let query = `g.V().has('uri', '${uri}')${skipDisabled ? '.' + _this5.getEnabledNodeFilter() : ''}`;

      let nodes = yield _this5.queryGraphNodes(query, context);

      if (nodes.length === 1) {
        return nodes[0];
      }
    })();
  }

  /**
   * Update a node.
   *
   * @param {string} nodeType type of node
   * @param {string} id Id of the node
   * @param {object} properties to update
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Promise<Node,Error>} A promise that returns a node object
   */
  updateNode(nodeType, id, properties, context) {
    return this.publish('node.update', {
      _id: id,
      nodeType: _models.Node.normalizedNodeType(nodeType),
      _origin: _models.Node.getOrigin(),
      _source: properties
    }, context).then(node => (0, _models.createNode)(node.nodeType, node._id, node._source, null, node.uri));
  }

  /**
   * Disable a node
   *
   * @param {string} nodeType type of node
   * @param {string} id Id of the node
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Promise.<boolean, Error>} A promise that returns true if deleted, false otherwise.
   */
  disableNode(nodeType, id, context) {
    var _this6 = this;

    return _asyncToGenerator(function* () {
      yield _this6.updateNode(nodeType, id, { _enabled: false, lastUpdate: new Date().getTime() }, context);
      return true;
    })();
  }

  /**
   * Enable a node
   * @param {string} nodeType type of node
   * @param {string} id Id of the node
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Promise.<Node, Error>} A promise that returns true if deleted, false otherwise.
   */
  enableNode(nodeType, id, context) {
    var _this7 = this;

    return _asyncToGenerator(function* () {
      return _this7.updateNode(nodeType, id, { _enabled: false }, context);
    })();
  }

  /**
   * Delete a node
   *
   * @param {string} id Id of the node
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Promise.<boolean, Error>} A promise that returns true if deleted, false otherwise.
   */
  deleteNode(id, context) {
    return this.publish('node.delete', id, context);
  }

  /**
   * Create an edge between two nodes.
   *
   * @param {string} subj Subject node id (edge OUT direction)
   * @param {string} pred Relation name
   * @param {string} obj  Object node id (edge IN direction)
   * @param {list}   properties Edge properties
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Promise.<Edge, Error>} A promise that returns an edge object
   */
  createEdge(subj, pred, obj, properties, context) {
    return this.publish('edge.create', _models.Edge.serializePartial(subj, pred, obj, properties), context).then(edge => (0, _models.createEdge)(edge._id, edge.subj, edge.pred, edge.obj, edge._source));
  }

  /**
   * Get edges for following triplet.
   *
   * @param {string} subj Subject node id (edge OUT direction)
   * @param {string} pred Edge label
   * @param {string} obj Object node id (edge IN direction)
   * @param {list}   properties Edge properties
   * @param {object} context Context information to add as AMQP headers.
   * @param {boolean} skipDisabled skip node if disabled
   * @returns {Promise.<Edge[], Error>|*} A promise that returns a list of edges.
   */
  getEdges(subj, pred, obj, properties, context, skipDisabled = true) {
    let query = `g.V('${subj}').outE('${pred}').where(__.inV().hasId('${obj}'))${skipDisabled ? '.' + this.getEnabledNodeFilter() : ''}.dedup()`;

    return this.queryGraphEdges(query, context);
  }

  /**
   * Create an edge between two nodes.
   *
   * @param {string} id edge id
   * @param {string} subj Subject node id (edge OUT direction)
   * @param {string} pred Relation name
   * @param {string} obj  Object node id (edge IN direction)
   * @param {list}   properties Edge properties
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Promise.<Edge, Error>} A promise that returns an edge object
   */
  updateEdge(id, subj, pred, obj, properties = [], context) {
    return this.publish('edge.update', _models.Edge.serializePartial(subj, pred, properties), context).then(edge => (0, _models.createEdge)(edge._id, edge.subj, edge.pred, edge.obj, edge._source));
  }

  /**
   * Delete an edge
   *
   * @param {string} subj Subject node id (edge OUT direction)
   * @param {string} pred Relation name
   * @param {string} obj  Object node id (edge IN direction)
   * @param {list}   properties Edge properties
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Promise.<boolean, Error>} A promise that returns true if deleted, false otherwise.
   */
  deleteEdges(subj, pred, obj, properties, context) {
    return this.publish('edges.delete', _models.Edge.serializePartial(subj, pred, obj, properties), context);
  }

  /**
   * Delete edges following triple.
   *
   * @param {string} id
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Promise.<boolean, Error>} A promise that returns true if deleted, false otherwise.
   */
  deleteEdge(id, context) {
    return this.publish('edge.delete', id, context);
  }

  /**
   *
   * @param id
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Promise.<Edge,Error>} A promise that returns
   */
  getEdge(id, context) {
    return this.publish('edge.get', id, context).then(edge => (0, _models.createEdge)(edge._id, edge.subj, edge.pred, edge.obj, edge._source));
  }

  /**
   * Get edge by labels
   *
   * @param labelTypes
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Promise.<Edge,Error>} A promise that returns
   */
  getEdgesByLabel(labelTypes, context) {
    if (typeof labelTypes === 'string') {
      labelTypes = [_models.Edge.normalizePredicate(labelTypes)];
    } else {
      labelTypes = labelTypes.map(labelType => _models.Edge.normalizePredicate(labelType));
    }

    return this.publish('edges.byLabel', labelTypes, context).then(edges => edges.map(edge => (0, _models.createEdge)(edge._id, edge.subj, edge.pred, edge.obj, edge._source)));
  }

  /**
   * Create a graph
   *
   * @param {Node[]} nodes List of graph nodes
   * @param {Edge[]} edges List of graph edges
   * @param {object} context Context information to add as AMQP headers.
   * @param {boolean} isRetry
   * @returns {Graph} A promise that returns a graph object
   */
  createGraph(nodes, edges = [], context, isRetry = false) {
    var _this8 = this;

    return _asyncToGenerator(function* () {
      let graph;

      try {
        graph = yield _this8.publish('graph.create', {
          nodes,
          edges
        }, context);
        // Simulate a transaction.
      } catch (e) {
        _winstonColor2.default.warn("graph.create error on", {
          nodes,
          edges
        }, e);

        let creatingNodes = Object.values(nodes);

        for (let creatingNode of creatingNodes) {
          if (!creatingNode._id && creatingNode.uri) {
            let node = yield _this8.getNodeByURI(creatingNode.uri, context, false);

            if (node) {
              _winstonColor2.default.warn("Removing node ", creatingNode.uri, node.id);

              yield _this8.deleteNode(node.id, context);
            }
          }
        }

        if (isRetry) {
          throw e;
        } else {
          _winstonColor2.default.warn("Retry...");
          return _this8.createGraph(nodes, edges, context, true);
        }
      }

      let createdNodes = {};

      Object.keys(graph.nodes).map(function (nodeKey) {
        let node = graph.nodes[nodeKey];
        createdNodes[nodeKey] = (0, _models.createNode)(node.nodeType, node._id, node._source, null, node.uri);
      });

      return {
        nodes: createdNodes,
        edges: graph.edges.map(function (edge) {
          return (0, _models.createEdge)(edge._id, edge.subj, edge.pred, edge.obj, edge._source);
        })
      };
    })();
  }
}
exports.default = GraphstorePublisher;