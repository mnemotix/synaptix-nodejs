'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _AbstractConsumer = require('../AbstractConsumer');

var _AbstractConsumer2 = _interopRequireDefault(_AbstractConsumer);

var _models = require('./models');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 *  Class used to synchronize data throw AMQP bus.
 *
 *  @extends AbstractConsumer
 */

/**
* This file is part of the Koncept package.
*
* Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
*
* Date : 13/04/2016
*/

class GraphstoreConsumer extends _AbstractConsumer2.default {

  getName() {
    return 'graphstore';
  }

  getRoutingKeys() {
    return ['sync.node.*'];
  }

  onMessage(msg) {
    msg = JSON.parse(msg.content.toString());

    let data = msg.body;

    switch (msg.command) {
      case "sync.node.created":
      case "sync.node.updated":
        this.dispatch(msg.command, (0, _models.createNode)(data.nodeType, data._id, data._source, data._meta, data.uri));
        break;
      case "sync.node.deleted":
        this.dispatch(msg.command, (0, _models.createNode)(data.nodeType, data._id, data._source, data._meta, data.uri));
        break;
    }
  }

  bindOnCreate(callback) {
    this.bind("sync.node.created", callback);
  }

  bindOnUpdate(callback) {
    this.bind("sync.node.updated", callback);
  }

  bindOnDelete(callback) {
    this.bind("sync.node.deleted", callback);
  }
}
exports.default = GraphstoreConsumer;