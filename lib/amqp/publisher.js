'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _v = require('uuid/v4');

var _v2 = _interopRequireDefault(_v);

var _callbackStore = require('callback-store');

var _callbackStore2 = _interopRequireDefault(_callbackStore);

var _createConnection = require('./create-connection');

var _createConnection2 = _interopRequireDefault(_createConnection);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = (() => {
  var _ref = _asyncToGenerator(function* (opt = {}) {
    const { publisherName, ttl } = opt;
    const { connection, exchange } = yield (0, _createConnection2.default)(opt);

    const resultsQueueName = `${process.env.UUID || (0, _v2.default)()}_pid:${process.pid}_publisher:${publisherName}_callback_queue`;

    const callbacks = (0, _callbackStore2.default)();

    const exchangeOptions = _extends({
      exclusive: false,
      autoDelete: true
    }, opt.exchangeOptions);

    function onResult(message, headers, deliveryInfo) {
      const cb = callbacks.get(deliveryInfo.correlationId);
      if (!cb) return;

      const args = [].concat(message);

      cb.apply(null, args);
    }

    /**
     * call a remote command
     *
     * @param {string} cmd   command name
     * @param {Buffer|Object|String}params    parameters of command
     * @param {function} cb        callback
     * @param {object} options   advanced options of amqp
     */
    function call(cmd, params, cb, options) {
      options = options || {};
      options.contentType = 'application/json';

      if (!cb) {
        exchange.publish(cmd, params, options);
        return;
      }

      const corrId = (0, _v2.default)();
      callbacks.add(corrId, cb, ttl || 5e3);

      options.mandatory = true;
      options.replyTo = resultsQueueName;
      options.correlationId = corrId;

      exchange.publish(cmd, params, options, err => {
        if (err) {
          const cb = callbacks.get(corrId);
          cb(err);
        }
      });
    }

    return new Promise(function (resolve, reject) {
      connection.queue(resultsQueueName, exchangeOptions, function (queue) {
        queue.subscribe(onResult);
        queue.bind(exchange, resultsQueueName);
        resolve({ call });
      });
    });
  });

  return function () {
    return _ref.apply(this, arguments);
  };
})();