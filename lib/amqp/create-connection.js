'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _amqp = require('amqp');

var _amqp2 = _interopRequireDefault(_amqp);

var _winstonColor = require('winston-color');

var _winstonColor2 = _interopRequireDefault(_winstonColor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let connect = function (options, cb) {
  return new Promise((resolve, reject) => {
    if (!options.uri && !options.host) {
      options.uri = 'amqp://guest:guest@localhost:5672';
    }

    let attempts = 1;

    const connection = _amqp2.default.createConnection(options);

    connection.on('ready', () => {
      attempts = 1;

      connection.exchange(options.exchangeName, {
        autoDelete: false
      }, exchange => {
        resolve({
          exchange,
          connection
        });
      });
    });

    connection.on('error', e => {
      _winstonColor2.default.error("AMQP broker error:", e);

      if (attempts < 5) {
        _winstonColor2.default.info(`Attempt reconnection in ${attempts} seconds...`);
        setTimeout(() => {
          attempts++;
          connection.reconnect();
        }, 1000 * attempts);
      }
    });
  });
};

exports.default = (options, cb) => {
  return connect(options, cb);
};