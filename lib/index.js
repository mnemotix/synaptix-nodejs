'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DseNode = exports.DseEdge = exports.createDseNode = exports.createDseEdge = exports.cloudsyncClient = exports.keycloakClient = exports.indexPublisher = exports.graphStoreDsePublisher = exports.uriGenPublisher = exports.CloudsyncClient = exports.KeycloakClient = exports.IndexConsumer = exports.IndexPublisher = exports.GraphStoreDseConsumer = exports.GraphStoreDsePublisher = exports.UriGenPublisher = undefined;

var _UrigenPublisher = require('./urigen/UrigenPublisher');

var _UrigenPublisher2 = _interopRequireDefault(_UrigenPublisher);

var _GraphstorePublisher = require('./graphstore/GraphstorePublisher');

var _GraphstorePublisher2 = _interopRequireDefault(_GraphstorePublisher);

var _GraphstoreConsumer = require('./graphstore/GraphstoreConsumer');

var _GraphstoreConsumer2 = _interopRequireDefault(_GraphstoreConsumer);

var _IndexPublisher = require('./index/IndexPublisher');

var _IndexPublisher2 = _interopRequireDefault(_IndexPublisher);

var _IndexConsumer = require('./index/IndexConsumer');

var _IndexConsumer2 = _interopRequireDefault(_IndexConsumer);

var _KeycloakClient = require('./keycloak/KeycloakClient');

var _KeycloakClient2 = _interopRequireDefault(_KeycloakClient);

var _CloudsyncClient = require('./cloudsync/CloudsyncClient');

var _CloudsyncClient2 = _interopRequireDefault(_CloudsyncClient);

var _models = require('./graphstore/models');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const uriGenPublisher = new _UrigenPublisher2.default(process.env.UUID),
      graphStoreDsePublisher = new _GraphstorePublisher2.default(process.env.UUID),
      indexPublisher = new _IndexPublisher2.default(process.env.UUID),
      keycloakClient = new _KeycloakClient2.default(process.env.OAUTH_REALM, process.env.UUID),
      cloudsyncClient = new _CloudsyncClient2.default(process.env.UUID);

exports.UriGenPublisher = _UrigenPublisher2.default;
exports.GraphStoreDsePublisher = _GraphstorePublisher2.default;
exports.GraphStoreDseConsumer = _GraphstoreConsumer2.default;
exports.IndexPublisher = _IndexPublisher2.default;
exports.IndexConsumer = _IndexConsumer2.default;
exports.KeycloakClient = _KeycloakClient2.default;
exports.CloudsyncClient = _CloudsyncClient2.default;
exports.uriGenPublisher = uriGenPublisher;
exports.graphStoreDsePublisher = graphStoreDsePublisher;
exports.indexPublisher = indexPublisher;
exports.keycloakClient = keycloakClient;
exports.cloudsyncClient = cloudsyncClient;
exports.createDseEdge = _models.createEdge;
exports.createDseNode = _models.createNode;
exports.DseEdge = _models.Edge;
exports.DseNode = _models.Node;