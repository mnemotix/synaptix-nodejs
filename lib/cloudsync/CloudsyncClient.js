'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   * This file is part of the synaptix-nodejs package.
                                                                                                                                                                                                                                                                   *
                                                                                                                                                                                                                                                                   * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
                                                                                                                                                                                                                                                                   *
                                                                                                                                                                                                                                                                   * Date : 31/05/2016
                                                                                                                                                                                                                                                                   */

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _pixlXml = require('pixl-xml');

var _pixlXml2 = _interopRequireDefault(_pixlXml);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _objectPath = require('object-path');

var _objectPath2 = _interopRequireDefault(_objectPath);

var _winstonColor = require('winston-color');

var _winstonColor2 = _interopRequireDefault(_winstonColor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

//request.debug = true;

/**
 * Cloud client
 */
class CloudsyncClient {
  getAuthorizationHeader(asRoot = false) {
    let login = asRoot ? process.env.CLOUD_ROOT_LOGIN : process.env.CLOUD_ARCHIVE_LOGIN;
    let password = asRoot ? process.env.CLOUD_ROOT_PASSWORD : process.env.CLOUD_ARCHIVE_PASSWORD;

    return `Basic ${new Buffer(`${login}:${password}`).toString('base64')}`;
  }

  getRootSession() {
    return new Session({
      'Authorization': this.getAuthorizationHeader(true)
    });
  }

  getArchiveSession() {
    return new Session({
      'Authorization': this.getAuthorizationHeader(false)
    });
  }

  getUserSessionWithPassword(login, password) {
    return new Session({
      'Authorization': `Basic ${new Buffer(`${login}:${password}`).toString('base64')}`
    });
  }

  getUserSession(accessToken) {
    return new Session({
      // @see https://bitbucket.org/mnemotix/owncloud-user_keycloak
      'Authorization': `Basic Og==`,
      'Delegated-Authorization': `Bearer: ${accessToken}`
    });
  }
}

exports.default = CloudsyncClient; /**
                                    * Defines a cloud session.
                                    */

class Session {
  constructor(authHeaders = {}) {
    this.SHARE_TYPE_USER = 0;
    this.SHARE_TYPE_GROUP = 1;
    this.SHARE_TYPE_PUBLIC = 3;
    this.SHARE_TYPE_FEDERATED_CLOUD_SHARE = 6;
    this.SHARE_PERMISSION_READ = 1;
    this.SHARE_PERMISSION_UPDATE = 2;
    this.SHARE_PERMISSION_CREATE = 4;
    this.SHARE_PERMISSION_DELETE = 8;
    this.SHARE_PERMISSION_SHARE = 16;

    let host = process.env.CLOUD_BASE_URL || 'http://192.168.99.100:8082';

    this.baseWebDAV = `${host}/remote.php/webdav`;
    this.baseShareAPI = `${host}/ocs/v1.php/apps/files_sharing/api/v1`;
    this.baseCloudAPI = `${host}/ocs/v1.php/cloud`;

    this.authHeaders = authHeaders;
  }

  requestWebDAV(uri, options = {}) {
    let extraHeaders = {};

    if (options.headers) {
      extraHeaders = options.headers;
    }

    delete options.headers;

    return new Promise((resolve, reject) => {
      (0, _request2.default)(_extends({
        uri: `${this.baseWebDAV}${this.trulyEncodeUri(uri)}`,
        headers: _extends({}, this.authHeaders, extraHeaders),
        followRedirect: true,
        followAllRedirects: true,
        followOriginalHttpMethod: true
      }, options), (error, response, body) => {
        if (!error) {
          resolve(body, response);
        } else {
          reject(error);
        }
      });
    });
  }

  requestAPI(uri, options = {}) {
    return new Promise((resolve, reject) => {
      (0, _request2.default)(_extends({
        uri,
        headers: _extends({}, this.authHeaders),
        followRedirect: true,
        followAllRedirects: true,
        followOriginalHttpMethod: true,
        json: true
      }, options), (error, response, body) => {
        if (!error) {
          resolve(body, response);
        } else {
          reject(error);
        }
      });
    });
  }

  trulyEncodeUri(path) {
    return path.split('/').map(fragment => encodeURIComponent(fragment)).join('/');
  }

  requestShareAPI(uri, options = {}, asRoot = false) {
    return this.requestAPI(`${this.baseShareAPI}${uri}`, options, asRoot);
  }

  requestCloudAPI(uri, options = {}, asRoot = false) {
    return this.requestAPI(`${this.baseCloudAPI}${uri}`, options, asRoot);
  }

  ping(options = {}, asRoot) {
    return this.requestAPI(`${this.baseCloudAPI}`, options, asRoot);
  }

  propFind(path) {
    var _this = this;

    return _asyncToGenerator(function* () {
      try {
        var body = yield _this.requestWebDAV(path, {
          method: 'PROPFIND',
          body: `<?xml version="1.0"?>
<d:propfind  xmlns:d="DAV:" xmlns:oc="http://owncloud.org/ns">
  <d:prop>
    <d:getlastmodified />
    <d:getetag />
    <d:getcontenttype />
    <d:resourcetype />
    <oc:fileid />
    <oc:permissions />
    <oc:size />
    <d:getcontentlength />
    <oc:share-types />
  </d:prop>
</d:propfind>`
        });
      } catch (e) {
        _winstonColor2.default.error(e);
      }

      try {
        let parsing = _pixlXml2.default.parse(body);

        if (parsing && parsing['d:response']) {
          return parsing['d:response'].map(function (file) {
            let propstat,
                props = {};

            let href = file['d:href'].replace('remote.php/webdav/', '');

            if (file['d:propstat']) {
              if (file['d:propstat'].length) {
                propstat = file['d:propstat'].find(function (stats) {
                  return stats['d:status'] == "HTTP/1.1 200 OK";
                });
              } else {
                propstat = file['d:propstat'];
              }

              if (propstat && propstat['d:prop']) {
                props = {
                  size: propstat['d:prop']['oc:size'] || propstat['d:prop']['d:getcontentlength'],
                  internalId: propstat['d:prop']['oc:fileid'],
                  mtime: (0, _moment2.default)(propstat['d:prop']['d:getlastmodified']).unix() * 1000,
                  mime: propstat['d:prop']['d:getcontenttype'],
                  isDirectory: !!propstat['d:prop']['d:resourcetype'] && typeof propstat['d:prop']['d:resourcetype']['d:collection'] != "undefined"
                };
              }
            }

            let splittedHref = href.split("/"),
                name;

            if (props.isDirectory && splittedHref.length > 1) {
              name = splittedHref[splittedHref.length - 2];
            } else {
              name = splittedHref[splittedHref.length - 1];
            }

            return _extends({
              href,
              name: decodeURIComponent(name)
            }, props);
          });
        }
      } catch (e) {
        _winstonColor2.default.error(e);
      }
    })();
  }

  createFolder(path, raiseErrorIfAlreadyExists = false) {
    var _this2 = this;

    return _asyncToGenerator(function* () {
      if (!path || path === "/") {
        return;
      }

      let propfindRsp = yield _this2.requestWebDAV(path, {
        method: 'PROPFIND'
      });

      let parsing = _pixlXml2.default.parse(propfindRsp);

      if (parsing && parsing['d:response']) {
        _winstonColor2.default.debug(`WEBDAV: ${path} already exists, not need to recreate it.`);
        return;
      }

      let paths = path.split('/').splice(1),
          promise;

      paths.map(function (fragment, index) {
        let path = '/' + paths.slice(0, index + 1).join('/');

        if (!promise) {
          promise = _this2.requestWebDAV(path, {
            method: 'MKCOL'
          }).catch(function (err) {
            // 405 == Resource already exists
            if (err.statusCode !== 405 || raiseErrorIfAlreadyExists) {
              throw err;
            }
          });
        } else {
          promise = promise.then(function () {
            return _this2.requestWebDAV(path, {
              method: 'MKCOL'
            }).catch(function (err) {
              // 405 == Resource already exists
              if (err.statusCode !== 405 || raiseErrorIfAlreadyExists) {
                throw err;
              }
            });
          });
        }
      });

      return promise;
    })();
  }

  moveFolder(sourcePath, targetPath) {
    return this.moveFile(sourcePath, targetPath);
  }

  removeFolder(path) {
    return this.requestWebDAV(path, {
      method: 'DELETE'
    });
  }

  removeFile(path) {
    return this.requestWebDAV(path, {
      method: 'DELETE'
    });
  }

  getFileDownloadUrl(path) {
    return `${this.baseWebDAV}${path}`;
  }

  getFile(path) {
    return this.requestWebDAV(this._internalizePath(path), {
      method: 'GET'
    });
  }

  putFile(path, data) {
    let dirname = path.replace(/\/[^\/]*$/, '');

    return this.createFolder(dirname).then(() => {
      _winstonColor2.default.debug('WebDAV command PUT :' + path);
      return this.requestWebDAV(path, {
        method: 'PUT',
        body: data
      });
    });
  }

  moveFile(sourcePath, targetPath) {
    return this.requestWebDAV(sourcePath, {
      method: 'MOVE',
      headers: {
        'Destination': `${this.baseWebDAV}${this.trulyEncodeUri(targetPath)}`
      }
    });
  }

  streamFile(path, stream) {
    var _this3 = this;

    return _asyncToGenerator(function* () {
      let dirname = path.replace(/\/[^\/]*$/, '');

      yield _this3.createFolder(dirname);

      _winstonColor2.default.info('WebDAV command PUT (by streaming):' + path);

      return new Promise(function (resolve, reject) {
        stream.pipe(_request2.default.put({
          uri: `${_this3.baseWebDAV}${_this3.trulyEncodeUri(path)}`,
          headers: _extends({}, _this3.authHeaders),
          followRedirect: true,
          followAllRedirects: true,
          followOriginalHttpMethod: true
        }, function (error, response, body) {
          if (!error) {
            resolve(body, response);
          } else {
            reject(error);
          }
        }));
      });
    })();
  }

  sharePublicResource(path, options = {}, returnJustUrl = true) {
    return this.requestShareAPI('/shares?format=json', {
      method: 'POST',
      body: _extends({
        path: this._internalizePath(path),
        shareType: this.SHARE_TYPE_PUBLIC,
        publicUpload: false
      }, options)
    }).then(rsp => {
      if (_objectPath2.default.get(rsp, 'ocs.meta.statuscode', 500) < 400) {
        if (returnJustUrl) {
          return rsp.ocs.data.url + '/download';
        } else {
          let file = rsp.ocs.data;
          file.url += '/download';
          return file;
        }
      } else {
        _winstonColor2.default.error(`Something gone wrong with file sharing : ${JSON.stringify(rsp, null, ' ')}`);
        throw new Error("Something gone wrong with file sharing", rsp.ocs);
      }
    });
  }

  shareResourceWithGroup(path, groupId, options = {}, throwErrorIfGroupAlreadyShared = false) {
    return this.requestShareAPI('/shares?format=json', {
      method: 'POST',
      form: _extends({
        path: this._internalizePath(path),
        shareType: this.SHARE_TYPE_GROUP,
        shareWith: groupId,
        permissions: this.SHARE_PERMISSION_READ | this.SHARE_PERMISSION_CREATE | this.SHARE_PERMISSION_UPDATE | this.SHARE_PERMISSION_DELETE
      }, options)
    }).then(rsp => {
      switch (_objectPath2.default.get(rsp, 'ocs.meta.statuscode', 500)) {
        case 400:
          throw new Error("Something gone wrong with file sharing: 400 - Unknown share type");
          break;
        case 403:
          if (throwErrorIfGroupAlreadyShared) {
            throw new Error("Something gone wrong with file sharing: 403 - Path already shared with this group");
          }
          break;
        case 404:
          throw new Error("Something gone wrong with file sharing: 404 - Wrong path, file/folder doesn't exist");
          break;
      }

      return rsp.ocs;
    });
  }

  getResourcePublicUrl(path) {
    return this.requestShareAPI(`/shares?format=json&reshares=true&path=${this._internalizePath(path)}`, {
      method: 'GET'
    }).then(rsp => {
      if (_objectPath2.default.get(rsp, 'ocs.meta.statuscode', 500) < 400 && _objectPath2.default.get(rsp, 'ocs.data') && rsp.ocs.data.length > 0) {
        let share = rsp.ocs.data.find(share => share.share_type == 3);

        if (share) {
          return share.url + '/download';
        }
      }
    });
  }

  addArchiveUser() {
    return this.addUser(this.archiveLogin, this.archivePassword);
  }

  addUser(userId, password, throwErrorIfGroupExists = false) {
    return this.requestCloudAPI('/users?format=json', {
      method: 'POST',
      form: {
        userid: userId,
        password: password
      }
    }, true).then(rsp => {
      switch (_objectPath2.default.get(rsp, 'ocs.meta.statuscode', 500)) {
        case 101:
          throw new Error("Something gone wrong with user creation: 101 - Invalid input data");
          break;
        case 103:
          throw new Error("Something gone wrong with user creation: 103 - Unknown error occurred whilst adding the user");
          break;
        case 102:
          if (throwErrorIfGroupExists) {
            throw new Error("Something gone wrong with group creation: 102 - Username already exists");
          }
      }

      return true;
    });
  }

  addGroup(groupId, throwErrorIfGroupExists = false) {
    return this.requestCloudAPI('/groups?format=json', {
      method: 'POST',
      form: {
        groupid: groupId
      }
    }, true).then(rsp => {
      switch (_objectPath2.default.get(rsp, 'ocs.meta.statuscode', 500)) {
        case 101:
          throw new Error("Something gone wrong with group creation: 101 - Invalid input data");
          break;
        case 103:
          throw new Error("Something gone wrong with group creation: 103 - Failed to add the group");
          break;
        case 102:
          if (throwErrorIfGroupExists) {
            throw new Error("Something gone wrong with group creation: 102 - Group already exists");
          }
      }

      return true;
    });
  }

  removeGroup(groupId, throwErrorIfGroupNotExists = false) {
    return this.requestCloudAPI(`/groups/${groupId}?format=json`, {
      method: 'DELETE'
    }).then(rsp => {
      switch (_objectPath2.default.get(rsp, 'ocs.meta.statuscode', 500)) {
        case 102:
          throw new Error("Something gone wrong with group deletion: 101 - Failed to delete group");
          break;
        case 101:
          if (throwErrorIfGroupNotExists) {
            throw new Error("Something gone wrong with group deletion: 102 - Group does not exist");
          }
      }
      return true;
    }, true);
  }

  addUserToAdminGroup(userId) {
    return this.addUserToGroup(userId, 'admin');
  }

  addUserToGroup(userId, groupId) {
    return this.requestCloudAPI(`/users/${userId}/groups?format=json`, {
      method: 'POST',
      form: {
        groupid: groupId
      }
    }, true).then(rsp => {
      switch (_objectPath2.default.get(rsp, 'ocs.meta.statuscode', 500)) {
        case 101:
          throw new Error("Something gone wrong with user to group: 101 - No group specified");
        case 102:
          throw new Error("Something gone wrong with user to group: 102 - Group doesn't exists");
        case 103:
          throw new Error("Something gone wrong with user to group: 103 - User doesn't exists");
        case 104:
          throw new Error("Something gone wrong with user to group: 104 - Insufficient privileges");
        case 105:
          throw new Error("Something gone wrong with user to group: 104 - Failed to add user to group");
      }

      return true;
    });
  }

  removeUserFromGroup(userId, groupId) {
    return this.requestCloudAPI(`/users/${userId}/groups?format=json`, {
      method: 'DELETE',
      form: {
        groupid: groupId
      }
    }, true).then(rsp => {
      switch (_objectPath2.default.get(rsp, 'ocs.meta.statuscode', 500)) {
        case 101:
          throw new Error("Something gone wrong with user to group: 101 - No group specified");
        case 102:
          throw new Error("Something gone wrong with user to group: 102 - Group doesn't exists");
        case 103:
          throw new Error("Something gone wrong with user to group: 103 - User doesn't exists");
        case 104:
          throw new Error("Something gone wrong with user to group: 104 - Insufficient privileges");
        case 105:
          throw new Error("Something gone wrong with user to group: 104 - Failed to add user to group");
      }

      return true;
    });
  }

  /**
   *
   * @param req
   * @param res
   */
  proxifyRequest(req, res) {
    let url = this._internalizePath(req.query.url);

    return req.pipe((0, _request2.default)({
      url,
      headers: {
        'Authorization': `Basic Og==`,
        'Delegated-Authorization': `Bearer: ${this.accessToken}`
      }
    })).pipe(res);
  }

  _internalizePath(path) {
    return path.replace(/\/[^\/]*\/files/, '');
  }
}