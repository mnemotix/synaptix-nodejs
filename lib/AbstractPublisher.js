'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _publisher = require('./amqp/publisher');

var _publisher2 = _interopRequireDefault(_publisher);

var _v = require('uuid/v4');

var _v2 = _interopRequireDefault(_v);

var _winstonColor = require('winston-color');

var _winstonColor2 = _interopRequireDefault(_winstonColor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * This file is part of the Koncept package.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Date : 13/04/2016
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            */

exports.default = class {
  constructor(senderId) {
    this.senderId = senderId;
  }

  getName() {
    return (0, _v2.default)();
  }

  /**
   * Publish an RPC AMQP message.
   *
   * @param {string} command RPC command
   * @param {*} body RPC body
   * @param {object} context extra AMQP headers
   * @param options
   * @returns {Promise.<T>}
   */
  publish(command, body, context = {}, options = {}) {
    var _this = this;

    return _asyncToGenerator(function* () {
      if (_this.initing) {
        return new Promise(function (resolve, reject) {
          setTimeout(function () {
            return resolve(_this.publish(command, body, context, options));
          }, 200);
        });
      }

      if (!_this.amqpPublisher) {
        let host = process.env.RABBITMQ_HOST || 'localhost';
        let port = process.env.RABBITMQ_PORT || 5672;
        let login = process.env.RABBITMQ_LOGIN || 'guest';
        let password = process.env.RABBITMQ_PASSWORD || 'guest';
        let exchangeName = process.env.RABBITMQ_EXCHANGE_NAME || 'synaptix-local';

        _this.initing = true;
        _this.amqpPublisher = yield (0, _publisher2.default)(_extends({
          publisherName: _this.getName() + '-' + (0, _v2.default)(),
          host,
          port,
          login,
          password,
          exchangeName,
          ttl: 30e3
        }, options));

        _winstonColor2.default.info(`Synaptix module [${_this.getName()}] is connecting to broker with uri [rabbitmq://${login}:${password}@${host}:${port}/${exchangeName}] [OK]`);

        _this.initing = false;
      }

      return new Promise(function (resolve, reject) {
        let payload = {
          command,
          sender: _this.senderId || process.env.UUID || 'mnx:app:nodejs',
          date: Date.now(),
          context,
          body
        };

        _this.amqpPublisher.call(command, payload, _this.onCallResponse.bind(_this, resolve, reject, payload));
      });
    })();
  }

  onCallResponse(resolve, reject, payload, rsp) {
    //winston.debug('[cmd]', payload);

    if (rsp && rsp.data) {
      let data = JSON.parse(rsp.data);

      //winston.debug('[resp ok]', data);

      if (data && typeof data === "object") {
        // Is the response successful ?
        if (['ok', 'empty'].indexOf(data.status.toLowerCase()) !== -1) {
          resolve(data.body, rsp);
        } else {
          reject(JSON.stringify({ command: data.command, requestPayload: payload.body, status: data.status, error: data.body }, null, " "));
        }
      }
    }
    //winston.debug('[resp err]', rsp);

    reject(JSON.stringify({ command: payload.command, status: 'no_response', requestPayload: payload, error: { message: `Something is wrong with the RPC response :
${rsp}` } }, null, " "));
  }
};